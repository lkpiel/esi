$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/sales/creation_of_purchase_order.feature");
formatter.feature({
  "line": 1,
  "name": "Creation of Purchase Order",
  "description": "As a Rentit\u0027s customer\r\nSo that I start with the construction project\r\nI want hire all the required machinery",
  "id": "creation-of-purchase-order",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1216127481,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "Plant catalog",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 7,
  "name": "the following plant catalog",
  "rows": [
    {
      "cells": [
        "id",
        "name",
        "description",
        "price"
      ],
      "line": 8
    },
    {
      "cells": [
        "1",
        "Mini Excavator",
        "1.5 Tonne Mini excavator",
        "150.00"
      ],
      "line": 9
    },
    {
      "cells": [
        "2",
        "Mini Excavator",
        "3 Tonne Mini excavator",
        "200.00"
      ],
      "line": 10
    },
    {
      "cells": [
        "3",
        "Midi Excavator",
        "5 Tonne Midi excavator",
        "250.00"
      ],
      "line": 11
    },
    {
      "cells": [
        "4",
        "Midi Excavator",
        "8 Tonne Midi excavator",
        "300.00"
      ],
      "line": 12
    },
    {
      "cells": [
        "5",
        "Maxi Excavator",
        "15 Tonne Large excavator",
        "400.00"
      ],
      "line": 13
    },
    {
      "cells": [
        "6",
        "Maxi Excavator",
        "20 Tonne Large excavator",
        "450.00"
      ],
      "line": 14
    },
    {
      "cells": [
        "7",
        "HS Dumper",
        "1.5 Tonne Hi-Swivel Dumper",
        "150.00"
      ],
      "line": 15
    },
    {
      "cells": [
        "8",
        "FT Dumper",
        "2 Tonne Front Tip Dumper",
        "180.00"
      ],
      "line": 16
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "the following inventory",
  "rows": [
    {
      "cells": [
        "id",
        "plantInfo",
        "serialNumber",
        "equipmentCondition"
      ],
      "line": 18
    },
    {
      "cells": [
        "1",
        "1",
        "exc-mn1.5-01",
        "SERVICEABLE"
      ],
      "line": 19
    },
    {
      "cells": [
        "2",
        "2",
        "exc-mn3.0-01",
        "SERVICEABLE"
      ],
      "line": 20
    },
    {
      "cells": [
        "3",
        "3",
        "exc-md5.0-01",
        "SERVICEABLE"
      ],
      "line": 21
    },
    {
      "cells": [
        "4",
        "4",
        "exc-md8.0-01",
        "SERVICEABLE"
      ],
      "line": 22
    },
    {
      "cells": [
        "5",
        "5",
        "exc-max15-01",
        "SERVICEABLE"
      ],
      "line": 23
    },
    {
      "cells": [
        "6",
        "6",
        "exc-max20-01",
        "SERVICEABLE"
      ],
      "line": 24
    },
    {
      "cells": [
        "7",
        "7",
        "dmp-hs1.5-01",
        "SERVICEABLE"
      ],
      "line": 25
    },
    {
      "cells": [
        "8",
        "8",
        "dmp-ft2.0-01",
        "SERVICEABLE"
      ],
      "line": 26
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "a customer is in the \"Plant Catalog\" web page",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "no purchase order exists in the system",
  "keyword": "And "
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.the_following_plant_catalog(PlantInventoryEntry\u003e)"
});
formatter.result({
  "duration": 658569532,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.the_following_inventory(PlantInventoryItem\u003e)"
});
formatter.result({
  "duration": 57371282,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Plant Catalog",
      "offset": 22
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.a_customer_is_in_the_web_page(String)"
});
formatter.result({
  "duration": 3159178172,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.no_purchase_order_exists_in_the_system()"
});
formatter.result({
  "duration": 72533,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Querying the plant catalog for an excavator",
  "description": "",
  "id": "creation-of-purchase-order;querying-the-plant-catalog-for-an-excavator",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 31,
  "name": "the customer queries the plant catalog for an \"Excavator\" available from \"2017-09-20\" to \"2017-09-22\"",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "6 plants are shown",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Excavator",
      "offset": 47
    },
    {
      "val": "2017-09-20",
      "offset": 74
    },
    {
      "val": "2017-09-22",
      "offset": 90
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.the_customer_queries_the_plant_catalog_for_an_available_from_to(String,String,String)"
});
formatter.result({
  "duration": 517523832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "6",
      "offset": 0
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.plants_are_shown(int)"
});
formatter.result({
  "duration": 171688460,
  "status": "passed"
});
formatter.after({
  "duration": 75957301,
  "status": "passed"
});
formatter.before({
  "duration": 4958718,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "Plant catalog",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 7,
  "name": "the following plant catalog",
  "rows": [
    {
      "cells": [
        "id",
        "name",
        "description",
        "price"
      ],
      "line": 8
    },
    {
      "cells": [
        "1",
        "Mini Excavator",
        "1.5 Tonne Mini excavator",
        "150.00"
      ],
      "line": 9
    },
    {
      "cells": [
        "2",
        "Mini Excavator",
        "3 Tonne Mini excavator",
        "200.00"
      ],
      "line": 10
    },
    {
      "cells": [
        "3",
        "Midi Excavator",
        "5 Tonne Midi excavator",
        "250.00"
      ],
      "line": 11
    },
    {
      "cells": [
        "4",
        "Midi Excavator",
        "8 Tonne Midi excavator",
        "300.00"
      ],
      "line": 12
    },
    {
      "cells": [
        "5",
        "Maxi Excavator",
        "15 Tonne Large excavator",
        "400.00"
      ],
      "line": 13
    },
    {
      "cells": [
        "6",
        "Maxi Excavator",
        "20 Tonne Large excavator",
        "450.00"
      ],
      "line": 14
    },
    {
      "cells": [
        "7",
        "HS Dumper",
        "1.5 Tonne Hi-Swivel Dumper",
        "150.00"
      ],
      "line": 15
    },
    {
      "cells": [
        "8",
        "FT Dumper",
        "2 Tonne Front Tip Dumper",
        "180.00"
      ],
      "line": 16
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "the following inventory",
  "rows": [
    {
      "cells": [
        "id",
        "plantInfo",
        "serialNumber",
        "equipmentCondition"
      ],
      "line": 18
    },
    {
      "cells": [
        "1",
        "1",
        "exc-mn1.5-01",
        "SERVICEABLE"
      ],
      "line": 19
    },
    {
      "cells": [
        "2",
        "2",
        "exc-mn3.0-01",
        "SERVICEABLE"
      ],
      "line": 20
    },
    {
      "cells": [
        "3",
        "3",
        "exc-md5.0-01",
        "SERVICEABLE"
      ],
      "line": 21
    },
    {
      "cells": [
        "4",
        "4",
        "exc-md8.0-01",
        "SERVICEABLE"
      ],
      "line": 22
    },
    {
      "cells": [
        "5",
        "5",
        "exc-max15-01",
        "SERVICEABLE"
      ],
      "line": 23
    },
    {
      "cells": [
        "6",
        "6",
        "exc-max20-01",
        "SERVICEABLE"
      ],
      "line": 24
    },
    {
      "cells": [
        "7",
        "7",
        "dmp-hs1.5-01",
        "SERVICEABLE"
      ],
      "line": 25
    },
    {
      "cells": [
        "8",
        "8",
        "dmp-ft2.0-01",
        "SERVICEABLE"
      ],
      "line": 26
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "a customer is in the \"Plant Catalog\" web page",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "no purchase order exists in the system",
  "keyword": "And "
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.the_following_plant_catalog(PlantInventoryEntry\u003e)"
});
formatter.result({
  "duration": 24164256,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.the_following_inventory(PlantInventoryItem\u003e)"
});
formatter.result({
  "duration": 45704087,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Plant Catalog",
      "offset": 22
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.a_customer_is_in_the_web_page(String)"
});
formatter.result({
  "duration": 726338250,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPurchaseOrderSteps.no_purchase_order_exists_in_the_system()"
});
formatter.result({
  "duration": 57600,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Creating a Purchase Order",
  "description": "",
  "id": "creation-of-purchase-order;creating-a-purchase-order",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 35,
  "name": "the customer queries the plant catalog for an \"Excavator\" available from \"2017-09-20\" to \"2017-09-22\"",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "the customer selects a \"3 Tonne Mini excavator\"",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "a purchase order should be created with a total price of 600.00",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Excavator",
      "offset": 47
    },
    {
      "val": "2017-09-20",
      "offset": 74
    },
    {
      "val": "2017-09-22",
      "offset": 90
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.the_customer_queries_the_plant_catalog_for_an_available_from_to(String,String,String)"
});
formatter.result({
  "duration": 176022965,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3 Tonne Mini excavator",
      "offset": 24
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.the_customer_selects_a(String)"
});
formatter.result({
  "duration": 337122416,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "600.00",
      "offset": 57
    }
  ],
  "location": "CreationOfPurchaseOrderSteps.a_purchase_order_should_be_created_with_a_total_price_of(BigDecimal)"
});
formatter.result({
  "duration": 9954982,
  "status": "passed"
});
formatter.after({
  "duration": 27972255,
  "status": "passed"
});
});