package com.example.common.infrastructure.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by lkpiel on 4/28/2017.
 */
@Configuration
@EnableWebSecurity
@EnableWebMvc
public class SecurityConfiguration {
    @Autowired
    void configureAuthenticationSystem(AuthenticationManagerBuilder builder) throws Exception {
        builder.inMemoryAuthentication()
                .withUser("user1").password("user1").roles("USER1")
                .and()
                .withUser("user2").password("user2").roles("USER2")
                .and()
                .withUser("admin").password("admin").roles("ADMIN");
    }
    @Configuration
    class RestSecurityConfiguration extends WebSecurityConfigurerAdapter{
        @Override
        protected void configure(HttpSecurity http) throws Exception{
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, "/api**").permitAll()
                    .antMatchers("/api**").authenticated()
                    .and()
                    .httpBasic()
                    .authenticationEntryPoint((req, res, exc) ->
                        res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "FORBIDDEN"));
        }
    }
}

