package com.example.common.infrastructure.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lkpiel on 4/28/2017.
 */
@Controller
@CrossOrigin
public class AuthenticationController {
    @GetMapping("api/authenticate")
    public List<String> authenticate (){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = new ArrayList<>();
        if(principal instanceof UserDetails){
            UserDetails details = (UserDetails) principal;
            for(GrantedAuthority authority:details.getAuthorities()){
                roles.add(authority.getAuthority());
            }
        }
        return roles;


    }
}

