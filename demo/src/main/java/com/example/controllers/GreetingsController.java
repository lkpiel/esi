package com.example.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by lkpiel on 4/28/2017.
 */
@Controller
public class GreetingsController {
    @RequestMapping("/sayhello")
    public String sayHello(){
        return "hello";
    }
}
