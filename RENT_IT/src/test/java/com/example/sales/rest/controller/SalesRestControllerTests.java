package com.example.sales.rest.controller;

import com.example.DemoApplication;
import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.reservations.infrastructure.ReservationsIdentifierFactory;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.example.sales.application.services.PurchaseOrderValidator;
import com.example.sales.application.services.SalesService;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import com.example.sales.infrastructure.SalesIdentifierFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.DataBinder;
import org.springframework.web.context.WebApplicationContext;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by lkpiel on 3/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@DirtiesContext
public class SalesRestControllerTests {
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;
    @Autowired
    SalesIdentifierFactory salesIdentifierFactory;
    @Autowired
    ReservationsIdentifierFactory reservationsIdentifierFactory;
    @Autowired
    SalesService salesService;
    @Autowired
    PlantInventoryEntryRepository repo;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    @Autowired @Qualifier("_halObjectMapper")
    ObjectMapper mapper;
    List<PlantInventoryEntryDTO> plants;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetAllPlants() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/plants?name=Exc&startDate=2017-08-14&endDate=2017-08-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        assertThat(plants.size()).isEqualTo(2);


    }

    @Test
    public void testCreatePurchaseOrder() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/inventory/plants?name=Exc&startDate=2017-08-14&endDate=2017-08-25"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(0));
        order.setTotal(BigDecimal.ONE);
        BusinessPeriodDTO bp = new BusinessPeriodDTO();
        bp.setStartDate(LocalDate.now().plusDays(50));
        bp.setEndDate(LocalDate.now().plusDays(60));
        order.setRentalPeriod(bp);
        mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsBytes(order)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


    }

    @Test
    public void testGetAllOrders() throws Exception {
        MvcResult result2 = mockMvc.perform(get("/api/sales/orders"))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        List<PurchaseOrderDTO> pos = mapper.readValue(result2.getResponse().getContentAsString(), new TypeReference<List<PurchaseOrderDTO>>() {
        });
        assertThat(pos.size()).isEqualTo(1);
    }


    @Test
    public void testGetPurchaseOrderThatExistsWithId() throws Exception {

        PurchaseOrder purchaseOrder = purchaseOrderRepository.findAll().get(0);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrder);
        String id = purchaseOrderDTO.get_id();

        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/{id}", id))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

        PurchaseOrderDTO purchaseOrderDTOResponse = mapper.readValue(result2.getResponse().getContentAsString(), new TypeReference<PurchaseOrderDTO>() {
        });
        assertThat(purchaseOrderDTOResponse.equals(purchaseOrderDTO));
    }

    @Test
    public void testGetPurchaseOrderThatDoesNotExistWithId() throws Exception {
        String id = "xxx666xxx";
        MvcResult result2 = mockMvc.perform(get("/api/sales/orders/{id}", id))
                .andExpect(status().isNotFound())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();

    }


    @Test
    public void testPurchaseOrderValidator() throws Exception{
        PlantInventoryEntryDTO entryDTO = plantInventoryEntryAssembler.toResource(plantInventoryEntryRepo.findOne("1"));
        entryDTO.set_id(null);
        PurchaseOrder po = PurchaseOrder.of(null,null,
                plantInventoryEntryAssembler.toEntity(entryDTO),
                LocalDate.now(),LocalDate.now().minusDays(10), BigDecimal.valueOf(-1), POStatus.OPEN);
        DataBinder poBinder = new DataBinder(po);
        poBinder.addValidators(new PurchaseOrderValidator());
        poBinder.validate();

        assertThat(poBinder.getBindingResult().getFieldError("id").isBindingFailure());
        assertThat(poBinder.getBindingResult().getFieldError("plant.id").isBindingFailure());
        assertThat(poBinder.getBindingResult().getFieldError("reservation").isBindingFailure());
        assertThat(poBinder.getBindingResult().getFieldError("total").isBindingFailure());
    }


    @Test
    public void testPurchaseOrderAcceptance() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/inventory/plants?name=Exc&startDate=2017-09-14&endDate=2017-09-25"))
                .andReturn();
        plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() {
        });

        PurchaseOrderDTO order = new PurchaseOrderDTO();
        order.setPlant(plants.get(1));
        BusinessPeriodDTO bpDTO = BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now());
        order.setRentalPeriod(bpDTO);
        result = mockMvc.perform(post("/api/sales/orders")
                .content(mapper.writeValueAsString(order))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();

        order = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderDTO.class);

        assertThat(order.get_xlink("accept")).isNotNull();

        mockMvc.perform(post(order.get_xlink("accept").getHref()))
                .andReturn();
    }

}