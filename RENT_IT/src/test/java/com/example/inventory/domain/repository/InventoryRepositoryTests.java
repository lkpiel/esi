package com.example.inventory.domain.repository;

import com.example.DemoApplication;
import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.reservations.domain.model.PlantReservation;
import com.example.reservations.domain.repository.PlantReservationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryRepositoryTests {
    //DELETED THE SQL ANNOTATION BECAUSE IT IS RUN AUTMATICALLY
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;
    @Autowired
    PlantReservationRepository plantReservationRepo;
    @Autowired
    InventoryRepository inventoryRepo;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantInventoryEntryRepo.count()).isEqualTo(14l);
    }

    @Test
    public void findAvailableTest() {
        // find one info element
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("1");
        PlantInventoryEntry maintenancedEntry = plantInventoryEntryRepo.findOne("3");
        System.out.println("SEE ON ENTRY " + entry.getName());
        // find one excavator instance
        //PlantInventoryItem item = plantInventoryItemRepo.findAllByPlantInfo(entry).get(0);
        // assert that search by "exec" finds the same entry as searched for before (1st item in entry database BUT DOESN't contain the element which is UNSERVICIABLE repairable a
        // and hasn't a maintanance scheduled)
        System.out.println(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(1l), LocalDate.now().plusWeeks(2l)).keySet().iterator().next().getName());
        System.out.println(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(1l), LocalDate.now().plusWeeks(2l)).keySet().iterator().next().getName());
        //System.out.println(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(1l), LocalDate.now().plusWeeks(2l)).keySet().iterator().next().equals(maintenancedEntry));

        //assertThat(inventoryRepo.findAvailable("exc", LocalDate.of(2017,2,20), LocalDate.of(2017,2,25))).containsKey(entry).doesNotContainKey(maintenancedEntry);
        assertThat(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(1l), LocalDate.now().plusWeeks(2l))).containsKey(entry);
    }

    @Test
    public void findOneStrictTest() {
        // find one info element
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("1");
        PlantInventoryEntry maintenancedEntry = plantInventoryEntryRepo.findOne("3");

        //1. Kontrollib, et meetod ei leiaks planti, kui periood on vähem kui kolm nädalat praegusest, kuigi talle on vahepeal maintenance määratud. Loe ülesande juhendi alt Relaxed punkti.
        assertThat(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(1l), LocalDate.now().plusDays(10l))).doesNotContainKey(maintenancedEntry);
    }

    @Test
    public void findOneRelaxedTest() {
        // find one info element
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("1");
        PlantInventoryEntry maintenancedEntry = plantInventoryEntryRepo.findOne("3");

        BusinessPeriod bp = BusinessPeriod.of(LocalDate.now().plusDays(1l), LocalDate.now().plusDays(1l));
        PlantReservation po = PlantReservation.of("5", bp, plantInventoryItemRepo.findOne("5"), null);
        /*po.setPlant(plantInventoryItemRepo.findOne("5"));
        po.setSchedule(bp);*/
        plantReservationRepo.save(po);
//        MaintenanceTask task = MaintenanceTask.of("4", "repairing the unservicable", TypeOfWork.CORRECTIVE, BigDecimal.valueOf(400), po);
//
//        //task.setReservation(po);
//        maintenanceTaskRepo.save(task);

        //2. Kontrollib, et meetod leiaks planti, kui soovitud periood on rohkem kui kolme nädala pärast ja vahepeal on UNSERVICEABLE_REPAIRABLE plantile määratud maintenance.
        assertThat(inventoryRepo.findAvailable("exc", LocalDate.now().plusWeeks(4l), LocalDate.now().plusWeeks(5l))).containsKey(maintenancedEntry);
    }

    @Test
    public void getItemsAfterReservationTest() {
        // find one info element
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("2");
        PlantInventoryEntry maintenancedEntry = plantInventoryEntryRepo.findOne("3");
        // find one instance of it that corresponds to that entry
        PlantInventoryItem item = plantInventoryItemRepo.findAllByPlantInfo(entry).get(0);

        // hashMap containing entries and corresponding number of instances in the database
        Map itemsBeforeReservations = inventoryRepo.findAvailable("exc", LocalDate.of(2017, 2, 20), LocalDate.of(2017, 2, 25));
        System.out.println("HO MANY ITEMS BEFORE: " + String.valueOf(itemsBeforeReservations.get(entry)));
        // create a new reservation
        PlantReservation pr = PlantReservation.of("6", BusinessPeriod.of(LocalDate.of(2017, 2, 21), LocalDate.of(2017, 2, 25)), item, null);
        plantReservationRepo.save(pr);
        // get items after reservations
        Map itemsAfterReservations = inventoryRepo.findAvailable("exc", LocalDate.of(2017, 2, 20), LocalDate.of(2017, 2, 25));
        System.out.println("HO MANY ITEMS AFTER: " + String.valueOf(itemsAfterReservations.get(entry)));
        // there should be now one less item in the map that was queried before
        assertThat(itemsAfterReservations.get(entry)).isEqualTo((Integer) itemsBeforeReservations.get(entry) - 1);
    }

    @Test
    public void findNrOfCorrectiveRepairsInLastFiveYearsTest() {
        // get the plan for year 2013
//        MaintenancePlan mp = maintenancePlanRepo.findOneByYear(2013);
//        // get an example task
//        MaintenanceTask mt = maintenanceTaskRepo.findOne("3");
//        MaintenanceTask mt2 = maintenanceTaskRepo.findOne("1");
//        // add this task as an additional to this maintenance plan
//        mp.getTasks().add(mt);
//        mp.getTasks().add(mt2);
//        maintenancePlanRepo.save(mp);
//        // assert that plan now contains 2 tasks
//        assertThat(inventoryRepo.findNrOfCorrectiveRepairsInLastFiveYears().get(2013)).isEqualTo(2);
    }

    @Test
    public void findEntriesNotHiredLastSixMonthsTest() {
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("1");
        PlantInventoryItem item = plantInventoryItemRepo.findAllByPlantInfo(entry).get(0);

        PlantReservation po = PlantReservation.of("7", BusinessPeriod.of(LocalDate.of(2016, 2, 21), LocalDate.of(2016, 2, 25)), item, null);
        //po.setPlant(item);
        //po.setSchedule(BusinessPeriod.of(LocalDate.of(2016, 2, 21), LocalDate.of(2016, 2, 25)));
        plantReservationRepo.save(po);

        assertThat(inventoryRepo.findEntriesNotHiredLastSixMonths()).contains(entry);
    }

    @Test
    public void findItemsNotHiredLastSixMonthsTest() {
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne("1");
        List<PlantInventoryItem> items = plantInventoryItemRepo.findAllByPlantInfo(entry);

        for (PlantInventoryItem item : items) {
            PlantReservation po = PlantReservation.of("8", BusinessPeriod.of(LocalDate.of(2016, 2, 21), LocalDate.of(2016, 2, 25)), item, null);
            //po.setPlant(item);
            //po.setSchedule(BusinessPeriod.of(LocalDate.of(2016, 2, 21), LocalDate.of(2016, 2, 25)));
            plantReservationRepo.save(po);
            assertThat(inventoryRepo.findItemsNotHiredLastSixMonths()).contains(item);
        }
    }

    @Test
    public void findCorrectiveRepairCostsInLast5YearsTest() {
        // get years and costs
//        Map<Integer, BigDecimal> correctiveRepairCostsInLast5Years = inventoryRepo.findCorrectiveRepairCostsInLast5Years();
//        // add an example task to year 2013
//        MaintenanceTask task = MaintenanceTask.of("5", "repair of things", TypeOfWork.CORRECTIVE, BigDecimal.valueOf(300), null);
//       // task.setTypeOfWork(TypeOfWork.CORRECTIVE);
//        //task.setPrice(BigDecimal.valueOf(300));
//        MaintenancePlan mp = maintenancePlanRepo.findOneByYear(2013);
//        mp.getTasks().add(task);
//
//        // add one other task to test the method as well
//        MaintenanceTask taskNotCorrective = MaintenanceTask.of("6", "repair of other things", TypeOfWork.OPERATIVE, BigDecimal.valueOf(350), null);
//        //taskNotCorrective.setTypeOfWork(TypeOfWork.OPERATIVE);
//        //taskNotCorrective.setPrice(BigDecimal.valueOf(300));
//        mp.getTasks().add(taskNotCorrective);
//
//        maintenancePlanRepo.save(mp);
//        // query plans and prices again
//        Map<Integer, BigDecimal> correctiveRepairCostsInLast5YearsLater = inventoryRepo.findCorrectiveRepairCostsInLast5Years();
        // check whether we get 5 years of data
//        assertThat(correctiveRepairCostsInLast5YearsLater.size()).isEqualTo(correctiveRepairCostsInLast5Years.size()).isEqualTo(5);
//
//        // check that awesome stuff
//        assertThat(correctiveRepairCostsInLast5YearsLater.get(2013)).isEqualTo(correctiveRepairCostsInLast5Years.get(2013).
//                add(BigDecimal.valueOf(300).setScale(2, BigDecimal.ROUND_HALF_UP)));
    }




/*@Test
    public void queryByName() {
        assertThat(plantInventoryEntryRepo.findByNameContaining("Mini").size()).isEqualTo(2);
    }

    @Test
    public void findAvailableTest() {
        PlantInventoryEntry entry = plantInventoryEntryRepo.findOne(1l);
        PlantInventoryItem item = plantInventoryItemRepo.findOneByPlantInfo(entry);

        assertThat(inventoryRepo.findAvailablePlants(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
                .contains(entry);

        PlantReservation po = new PlantReservation();
        po.setPlant(item);
        po.setSchedule(BusinessPeriod.of(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 2, 25)));
        poRepo.save(po);

        assertThat(inventoryRepo.findAvailablePlants(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
                .doesNotContain(entry);
    }*/

}
