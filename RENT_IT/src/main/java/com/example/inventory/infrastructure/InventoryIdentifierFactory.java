package com.example.inventory.infrastructure;


import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Philosoraptor on 06/03/2017.
 */

@Service
public class InventoryIdentifierFactory {
    public String nextInventoryID() {
        return UUID.randomUUID().toString();
    }

}
