package com.example.inventory.domain.model;

/**
 * Created by lkpiel on 5/23/2017.
 */
public enum PlantStatus {
    IN_STOCK, DISPATCHED, DELIVERED, REJECTED_BY_CUSTOMER, RETURNED, IN_MAINTENANCE
}
