package com.example.inventory.domain.repository;

import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lkpiel on 2/17/2017.
 */
@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, String>{
    @Query("SELECT item FROM PlantInventoryItem item WHERE item.plantInfo = ?1")
    List<PlantInventoryItem> findAllByPlantInfo(PlantInventoryEntry entry);

    @Query("SELECT item FROM PlantInventoryItem item WHERE item.plantInfo.id = ?1 AND item.equipmentCondition = 'SERVICEABLE'")
    List<PlantInventoryItem> findAllServiceableByPlantInfo(String entry);
}
