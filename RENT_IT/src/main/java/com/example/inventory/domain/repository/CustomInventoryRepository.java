package com.example.inventory.domain.repository;

import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantInventoryItem;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 2/17/2017.
 */
public interface CustomInventoryRepository {
    Map<PlantInventoryEntry, Integer> findAvailable(String name, LocalDate startData, LocalDate endDate);
    List<PlantInventoryEntry> findEntriesNotHiredLastSixMonths();
    List<PlantInventoryItem> findItemsNotHiredLastSixMonths();


}
