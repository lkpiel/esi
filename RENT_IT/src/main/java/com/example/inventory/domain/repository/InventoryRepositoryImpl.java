package com.example.inventory.domain.repository;

import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 2/17/2017.
 */

public class InventoryRepositoryImpl implements CustomInventoryRepository {
    @Autowired
    EntityManager em;

    @Override
    public Map<PlantInventoryEntry, Integer> findAvailable(String name, LocalDate startDate, LocalDate endDate) {
        Map<PlantInventoryEntry, Integer> resultMap = new HashMap<>();

        /*if(startDate == null){
            startDate = LocalDate.now();
            System.out.println("JAAA JUMMALA HAIGEGEE111");
        }
        if(endDate == null){
            endDate = LocalDate.now().plusDays(1);
            System.out.println("JAAA JUMMALA HAIGEGEE2222");
        }*/

        if ((startDate.isEqual(LocalDate.now()) || startDate.isAfter(LocalDate.now())) && endDate.isAfter(LocalDate.now())) {
            List<Object[]> entries = em.createQuery("SELECT item.plantInfo, count(item.plantInfo) FROM PlantInventoryItem item WHERE LOWER(item.plantInfo.name) LIKE ?1 AND " +
                    "(item.equipmentCondition = 'SERVICEABLE' AND item NOT IN (SELECT r.plant FROM PlantReservation r WHERE (?2 <= r.schedule.endDate) AND (?3 >= r.schedule.startDate)))" +
                    " OR  (item.equipmentCondition = 'UNSERVICEABLE_REPAIRABLE' AND (?4 > ?5 )) GROUP BY item.plantInfo")
                    .setParameter(1, "%" + name.toLowerCase() + "%")
                    .setParameter(2, startDate)
                    .setParameter(3, endDate)
                    .setParameter(4, startDate)
                    .setParameter(5, LocalDate.now().plusWeeks(3l)).getResultList();

            resultMap = new HashMap<>(entries.size());
            for (Object[] result : entries)
                resultMap.put((PlantInventoryEntry) result[0], (int) (long) result[1]);
       /* Map<PlantInventoryEntry, Integer> countedEntries = new HashMap<>();
        for(PlantInventoryEntry entry: entries){
            Integer count = Integer.parseInt(em.createQuery("SELECT COUNT(item.plantInfo) FROM PlantInventoryItem item WHERE item.plantInfo = ?1").setParameter(1, entry).getSingleResult().toString());
            countedEntries.put(entry, count);
        }*/
        }

        return resultMap;
    }

//    @Override
//    public Map<Integer, Integer> findNrOfCorrectiveRepairsInLastFiveYears() {
//        Map<Integer, Integer> yearWithRepairCounts = new HashMap<>();
//        for(int year = LocalDate.now().getYear(); year >  LocalDate.now().getYear() - 5; year-- ){
//           yearWithRepairCounts.put(year, Integer.parseInt(
//                   em.createQuery("SELECT count(task) FROM MaintenanceTask task WHERE " +
//                   "task.typeOfWork = 'CORRECTIVE' AND task.id IN " +
//                   "(SELECT tasks.id FROM MaintenancePlan plan JOIN plan.tasks tasks WHERE plan.yearOfAction = ?1)")
//                   .setParameter(1, year).getSingleResult().toString()));
//        }
//        //System.out.println("Tasks " + em.createQuery("SELECT plan.tasks FROM MaintenancePlan plan WHERE plan.yearOfAction = 2013").getResultList().toString());
//        return yearWithRepairCounts;
//    }

    //List of items or entries?
    @Override
    public List<PlantInventoryEntry> findEntriesNotHiredLastSixMonths() {
        return em.createQuery(
                "SELECT item.plantInfo FROM PlantInventoryItem item WHERE item.plantInfo.id IN (" +
                        "SELECT pr.plant.id FROM PlantReservation pr WHERE pr.schedule.endDate NOT BETWEEN ?1 AND ?2)", PlantInventoryEntry.class)
                .setParameter(1, LocalDate.now().minusMonths(6l))
                .setParameter(2, LocalDate.now())
                .getResultList();
    }

    @Override
    public List<PlantInventoryItem> findItemsNotHiredLastSixMonths() {
        return em.createQuery(
                "SELECT item FROM PlantInventoryItem item WHERE item IN (" +
                        "SELECT pr.plant FROM PlantReservation pr WHERE pr.schedule.endDate NOT BETWEEN ?1 AND ?2)", PlantInventoryItem.class)
                .setParameter(1, LocalDate.now().minusMonths(6l))
                .setParameter(2, LocalDate.now())
                .getResultList();
    }


//    //List of items or entries?
//    @Override
//    public Map<Integer, BigDecimal> findCorrectiveRepairCostsInLast5Years() {
//        Map<Integer, BigDecimal> yearsWithRepairCosts = new HashMap<>();
//        // iterate over 5 years (starting from present year)
//        for(int year = LocalDate.now().getYear(); year >  LocalDate.now().getYear() - 5; year-- ){
//            // get all the tasks of this year
//            List<MaintenanceTask> tasks = em.createQuery("SELECT task FROM MaintenanceTask task WHERE task.typeOfWork = 'CORRECTIVE' " +
//                    "AND task.id IN (SELECT tasks.id FROM MaintenancePlan plan JOIN plan.tasks tasks WHERE plan.yearOfAction = ?1)")
//                    .setParameter(1, year).getResultList();
//            // init price
//            BigDecimal planCost = BigDecimal.valueOf(0);
//            for(MaintenanceTask task : tasks){
//                planCost = planCost.add(task.getPrice());
//            }
//            yearsWithRepairCosts.put(year, planCost);
//            System.out.println("Year: " + year + "costs: " + yearsWithRepairCosts.get(year));
//        }
//        return yearsWithRepairCosts;
//    }


    //SELECT  r.plantInfo FROM PIItem i where i.plantInfo.name like ?1 and i not in (select Reservation.plant FROM PlantReservation r where not(?2 < r.schedule.endDate) OR (?3 > r.schedule.startDate
}
