package com.example.inventory.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity     // to tell Spring that this is an entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class PlantInventoryItem {
    //Marked this property as @Id and specify that you want JPA to use an ID generator
    @Id // for persitence
    String id;
    String serialNumber;

    // Select the format used for storing the enumeration (i.e. integer vs. string)
    @Enumerated(EnumType.STRING) // changes the way how internally in database things will be
    EquipmentCondition equipmentCondition;

    // Specify the cardinality and direction of the relationship
    @ManyToOne // because there may be many items referencing one entry
    PlantInventoryEntry plantInfo;

    @Enumerated(EnumType.STRING) // changes the way how internally in database things will be
    PlantStatus plantStatus;

    public void updateStatus(PlantStatus plantStatus) {
        this.plantStatus = plantStatus;
    }


}
