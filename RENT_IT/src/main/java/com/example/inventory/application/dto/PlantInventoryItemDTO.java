package com.example.inventory.application.dto;

import com.example.inventory.domain.model.EquipmentCondition;
import com.example.inventory.domain.model.PlantStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Philosoraptor on 22/03/2017.
 */

@Data
@EqualsAndHashCode
public class PlantInventoryItemDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    String serialNumber;
    EquipmentCondition equipmentCondition;
    PlantInventoryEntryDTO plantInfo;
    PlantStatus plantStatus;
}
