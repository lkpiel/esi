package com.example.inventory.application.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import java.math.BigDecimal;

/**
 * Created by Philosoraptor on 07/03/2017.
 */

@Data
@Getter
@EqualsAndHashCode
public class PlantInventoryEntryDTO extends com.example.common.rest.ResourceSupport {
    private String _id;
    private String name;
    private String description;
    @Column(precision = 8, scale = 2)
    private BigDecimal price;
}
