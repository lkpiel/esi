package com.example.inventory.application.services;

import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.common.application.services.BusinessPeriodValidator;
import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.dto.PlantInventoryItemDTO;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.inventory.domain.model.PlantStatus;
import com.example.inventory.domain.repository.InventoryRepository;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.inventory.infrastructure.InventoryIdentifierFactory;
import com.example.reservations.domain.model.PlantReservation;
import com.example.reservations.domain.repository.PlantReservationRepository;
import com.example.reservations.infrastructure.ReservationsIdentifierFactory;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 3/8/2017.
 */
@Service
public class InventoryService {

    @Autowired
    ReservationsIdentifierFactory reserveIdFactory;
    @Autowired
    InventoryRepository inventoryRepository;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    InventoryIdentifierFactory identifierFactory;
    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public PlantReservation createPlantReservation(String entryID, LocalDate startDate, LocalDate endDate, PurchaseOrder po) throws PlantNotFoundException {
        List<PlantInventoryItem> availableItems = plantReservationRepository.findAvailableItems(entryID, startDate, endDate);

        if (!availableItems.isEmpty()) {
            PlantInventoryItem item = availableItems.get(0);
            BusinessPeriod bp = BusinessPeriod.of(startDate, endDate);

            DataBinder bpBinder = new DataBinder(bp);
            bpBinder.addValidators(new BusinessPeriodValidator());
            bpBinder.validate();

            String id = reserveIdFactory.nextReservationID();
            PlantReservation reservation = PlantReservation.of(id, bp, item, null);
            if (!bpBinder.getBindingResult().hasErrors()) {
                plantReservationRepository.save(reservation);
            }
            return reservation;
        } else {
            System.out.println("PLANT IS NOT AVAILABLE ANYMORE. RESERVATION NOT CREATED!");
            throw new PlantNotFoundException("Requested plant is unavailable");
        }
    }


    public List<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        Map<PlantInventoryEntry, Integer> available = inventoryRepository.findAvailable(name, startDate, endDate);
        List<PlantInventoryEntry> plantInventoryEntryList = new ArrayList(available.keySet());
        return plantInventoryEntryAssembler.toResources(plantInventoryEntryList);
    }


    public List<PlantInventoryItemDTO> findAllServiceableByPlantInfo(String plantInfo) {
        List<PlantInventoryItem> plantInventoryItems = plantInventoryItemRepository.findAllServiceableByPlantInfo(plantInfo);
        System.out.println("NII MITU LEIDSIN " + plantInventoryItems.size() + " Selle plantinfoga " + plantInfo);
        return plantInventoryItemAssembler.toResources(plantInventoryItems);
    }

    public PlantInventoryItemDTO updatePlantItemStatus(String id, PlantStatus status) {
        PlantInventoryItem plantInventoryItem = plantInventoryItemRepository.findOne(id);

        if (plantInventoryItem.getPlantStatus().equals(PlantStatus.IN_STOCK)) {
            if (status.equals(PlantStatus.DISPATCHED)) {
                plantInventoryItem.updateStatus(status);
            }
        } else if (plantInventoryItem.getPlantStatus().equals(PlantStatus.DISPATCHED)) {
            if (status.equals(PlantStatus.DELIVERED)) {
                plantInventoryItem.updateStatus(status);
            }
        } else if (plantInventoryItem.getPlantStatus().equals(PlantStatus.DELIVERED)) {
            if (status.equals(PlantStatus.REJECTED_BY_CUSTOMER)||status.equals(PlantStatus.RETURNED)){
                plantInventoryItem.updateStatus(status);
            }
        } else if (plantInventoryItem.getPlantStatus().equals(PlantStatus.RETURNED)){
            if (status.equals(PlantStatus.IN_MAINTENANCE)||status.equals(PlantStatus.IN_STOCK)) {
                plantInventoryItem.updateStatus(status);
            }
        } else if (plantInventoryItem.getPlantStatus().equals(PlantStatus.IN_MAINTENANCE)||
                   plantInventoryItem.getPlantStatus().equals(PlantStatus.REJECTED_BY_CUSTOMER)) {
            if (status.equals(PlantStatus.IN_STOCK)) {
                plantInventoryItem.updateStatus(status);
            }
        }

        plantInventoryItemRepository.save(plantInventoryItem);
        return plantInventoryItemAssembler.toResource(plantInventoryItem);
    }
}