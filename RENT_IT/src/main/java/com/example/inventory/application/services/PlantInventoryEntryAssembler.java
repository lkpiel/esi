package com.example.inventory.application.services;

import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.rest.controller.InventoryRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lkpiel on 3/8/2017.
 */
@Service
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {

    public PlantInventoryEntryAssembler() {
        super(InventoryRestController.class, PlantInventoryEntryDTO.class);
    }

    @Override
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
        PlantInventoryEntryDTO dto = createResourceWithId(plant.getId(), plant);
        dto.set_id(plant.getId());
        dto.setName(plant.getName());
        dto.setDescription(plant.getDescription());
        dto.setPrice(plant.getPrice());
        return dto;
    }

    public List<PlantInventoryEntryDTO> toResources(List<PlantInventoryEntry> plants) {
        return plants.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }

    public PlantInventoryEntry toEntity(PlantInventoryEntryDTO plant) {

        try {
            System.out.println(plant.get_id());
            System.out.println(plant.getName());
            System.out.println(plant.getDescription());
            System.out.println(String.valueOf(plant.getPrice()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        PlantInventoryEntry plantInventoryEntry = PlantInventoryEntry.of(plant.get_id(), plant.getName(), plant.getDescription(), plant.getPrice());
        return plantInventoryEntry;
    }

    public List<PlantInventoryEntry> toEntity(List<PlantInventoryEntryDTO> plants) {
        return plants.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }
}
