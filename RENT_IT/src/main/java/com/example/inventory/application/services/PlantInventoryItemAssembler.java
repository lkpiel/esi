package com.example.inventory.application.services;

import com.example.inventory.application.dto.PlantInventoryItemDTO;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.inventory.rest.controller.InventoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by Philosoraptor on 22/03/2017.
 */
@Service
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    public PlantInventoryItemAssembler() {
        super(InventoryRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem plantInventoryItem) {
        PlantInventoryItemDTO dto = createResourceWithId(plantInventoryItem.getId(), plantInventoryItem);
        dto.set_id(plantInventoryItem.getId());
        dto.setEquipmentCondition(plantInventoryItem.getEquipmentCondition());
        dto.setPlantInfo(plantInventoryEntryAssembler.toResource(plantInventoryItem.getPlantInfo()));
        dto.setSerialNumber(plantInventoryItem.getSerialNumber());
        dto.setPlantStatus(plantInventoryItem.getPlantStatus());
        return dto;
    }

    public PlantInventoryItem toEntity(PlantInventoryItemDTO plantInventoryItemDTO){
        // TODO: don't know why this thingie can be null :S:S:S:S
        if (plantInventoryItemDTO != null) {
            PlantInventoryItem plantInventoryItem = PlantInventoryItem.of(
                    plantInventoryItemDTO.get_id(),
                    plantInventoryItemDTO.getSerialNumber(),
                    plantInventoryItemDTO.getEquipmentCondition(),
                    plantInventoryEntryAssembler.toEntity(plantInventoryItemDTO.getPlantInfo()),
                    plantInventoryItemDTO.getPlantStatus()
            );
            return  plantInventoryItem;
        } else {
            return null;
        }
    }

}
