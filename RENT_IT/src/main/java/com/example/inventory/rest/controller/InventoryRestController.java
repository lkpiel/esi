package com.example.inventory.rest.controller;

import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.dto.PlantInventoryItemDTO;
import com.example.inventory.application.services.InventoryService;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.inventory.application.services.PlantInventoryItemAssembler;
import com.example.inventory.domain.model.PlantStatus;
import com.example.inventory.domain.repository.InventoryRepository;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Philosoraptor on 13/03/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {

    @Autowired
    InventoryService inventoryService;
    @Autowired
    InventoryRepository inventoryRepository;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        System.out.println("QUERY");
        return inventoryService.findAvailablePlants(plantName, startDate, endDate);
    }

    @GetMapping("/items")
    public List<PlantInventoryItemDTO> findAllByPlantInfo(
            @RequestParam(name = "plantInventoryEntry") String plantInfo) {
        return inventoryService.findAllServiceableByPlantInfo(plantInfo);
    }
    @GetMapping("/items/{id}")
    public PlantInventoryItemDTO findItemById(
            @PathVariable(value = "id") String id) {
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }



    @GetMapping("/{id}")
    public PlantInventoryEntryDTO findPlant(
            @PathVariable(value = "id") String id
    ) {
        return plantInventoryEntryAssembler.toResource(inventoryRepository.findOne(id));
    }
    @PostMapping("items/{id}/instock")
    public PlantInventoryItemDTO stockItem(
            @PathVariable(value = "id") String id
    ) {
        System.out.println("UPDATE TO DISPATCHED");
        inventoryService.updatePlantItemStatus(id, PlantStatus.IN_STOCK);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
    @PostMapping("items/{id}/dispatched")
    public PlantInventoryItemDTO dispatchItem(
            @PathVariable(value = "id") String id
    ) {
        System.out.println("UPDATE TO DISPATCHED");
        inventoryService.updatePlantItemStatus(id, PlantStatus.DISPATCHED);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
    @PostMapping("items/{id}/delivered")
    public PlantInventoryItemDTO deliverItem(
            @PathVariable(value = "id") String id
    ) {
        inventoryService.updatePlantItemStatus(id, PlantStatus.DELIVERED);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
    @PostMapping("items/{id}/rejectedbycustomer")
    public PlantInventoryItemDTO rejectByCustomerItem(
            @PathVariable(value = "id") String id
    ) {
        inventoryService.updatePlantItemStatus(id, PlantStatus.REJECTED_BY_CUSTOMER);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
    @PostMapping("items/{id}/inmaintenance")
    public PlantInventoryItemDTO sendToMaintenanceItem(
            @PathVariable(value = "id") String id
    ) {
        inventoryService.updatePlantItemStatus(id, PlantStatus.IN_MAINTENANCE);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
    @PostMapping("items/{id}/returned")
    public PlantInventoryItemDTO returnItem(
            @PathVariable(value = "id") String id
    ) {
        inventoryService.updatePlantItemStatus(id, PlantStatus.RETURNED);
        return plantInventoryItemAssembler.toResource(plantInventoryItemRepository.findOne(id));
    }
}
