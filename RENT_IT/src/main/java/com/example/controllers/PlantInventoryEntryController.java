package com.example.controllers;

import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.repository.InventoryRepository;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.reservations.domain.repository.PlantReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

/**
 * Created by lgarcia on 2/10/2017.
 */
@RestController
public class PlantInventoryEntryController {
    @Autowired
    InventoryRepository repo;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;
    @Autowired
    PlantReservationRepository plantReservationRepo;

    @GetMapping("/plants")
    public Map<PlantInventoryEntry, Integer> findAll() {
        /*BusinessPeriod bp = BusinessPeriod.of(LocalDate.now().plusDays(1l), LocalDate.now().plusDays(1l));
        MaintenanceTask task = maintenanceTaskRepo.findOne(4l);

        PlantReservation po = new PlantReservation();
        po.setPlant(plantInventoryItemRepo.findOne(5l));
        System.out.println(plantInventoryItemRepo.findOne(5l).getSerialNumber());
        po.setSchedule(bp);
        plantReservationRepo.save(po);
        task.setReservation(po);
        maintenanceTaskRepo.save(task);
        */

        return repo.findAvailable("exc", LocalDate.now().plusWeeks(4l), LocalDate.now().plusWeeks(5l));
    }
    @GetMapping("/findrepairs")
    public Map<Integer, Integer> findNrOfCorrectiveRepairsInLastFiveYears() {

        /*BusinessPeriod bp = BusinessPeriod.of(LocalDate.now(), LocalDate.now());
        PlantReservation po = new PlantReservation();
        po.setPlant(plantInventoryItemRepo.findOne(1l));
        po.setSchedule(bp);
        plantReservationRepo.save(po);*/
        return null;
    }
}
