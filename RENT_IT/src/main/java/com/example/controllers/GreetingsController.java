package com.example.controllers;

import com.example.inventory.domain.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by lgarcia on 2/10/2017.
 */
// let this controller be commented out. Otherwise it hinders the remittance http endpoints
//@Controller
public class GreetingsController {
    @Autowired
    InventoryRepository inventoryRepo;

    @GetMapping
    public String sayHello() {
        //inventoryRepo.findCorrectiveRepairCostsInLast5Years();
        return "hello";
    }
}
