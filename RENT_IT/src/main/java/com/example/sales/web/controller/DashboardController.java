package com.example.sales.web.controller;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.repository.InventoryRepository;
import com.example.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.sales.application.services.SalesService;
import com.example.sales.domain.model.POStatus;
import com.example.sales.web.dto.CatalogQueryDTO;
import com.example.sales.application.dto.PurchaseOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 3/7/2017.
 */
@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    @Autowired
    InventoryRepository inventoryRepo;
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    SalesService salesService;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @GetMapping("/catalog/form")
    public String getQueryForm(Model model) {
        // BusinessPeriodDTO bp = BusinessPeriodDTO.of();
        model.addAttribute("catalogQuery", new CatalogQueryDTO());
        return "dashboard/catalog/query-form";
    }

    @PostMapping("/catalog/query")
    String executeQuery(CatalogQueryDTO query, Model model) {
        Map<PlantInventoryEntry, Integer> results = inventoryRepo.findAvailable(query.getName(), query.getRentalPeriod().getStartDate(), query.getRentalPeriod().getEndDate());
        List<PlantInventoryEntryDTO> plantsDTO = new ArrayList<>();
        plantsDTO = plantInventoryEntryAssembler.toResources(new ArrayList(results.keySet()));

        BusinessPeriodDTO bpDTO = BusinessPeriodDTO.of(query.getRentalPeriod().getStartDate(), query.getRentalPeriod().getEndDate());
        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
        model.addAttribute("plants", plantsDTO);
        model.addAttribute("rentalPeriod", bpDTO);
        model.addAttribute("po", poDTO);
        return "dashboard/catalog/query-result";
    }


    @GetMapping("/orders")
    String createPO(PurchaseOrderDTO poDTO, BusinessPeriodDTO bpDTO, Model model) throws PlantNotFoundException, IOException, MessagingException {
        PlantInventoryEntry pie = plantInventoryEntryRepository.findOne(poDTO.getPlant().get_id());
        PlantInventoryEntryDTO pieDTO = plantInventoryEntryAssembler.toResource(pie);

        // get all the data from the submitted form. Make a new purchase order using only the entry and businessPeriod
        PurchaseOrderDTO newPurchaseOrderDTO = salesService.createPurchaseOrder(pieDTO, bpDTO);
        System.out.println("JOUJOU############################ " +
                newPurchaseOrderDTO.getPlant().getName() +
                newPurchaseOrderDTO.getPlant().getDescription() +
                newPurchaseOrderDTO.getPlant().get_id() +
                newPurchaseOrderDTO.getPlant().getPrice() +
                bpDTO.getStartDate() +
                bpDTO.getEndDate());

        if (newPurchaseOrderDTO.getStatus().equals(POStatus.PENDING)) {
            // it seems that the available item existed for this entry and a reservation was made
            System.out.println("STATUS PENDING AND READY TO GO JOUJOUJOU");
            model.addAttribute("response", "Purchase order created");
        } else if (newPurchaseOrderDTO.getStatus().equals(POStatus.REJECTED)) {
            System.out.println("STATUS REJECTED AND PACK LAHTII JOUJOUJOU");
            // it seems that the available item didn't exist for this entry and a reservation wasn't made
            model.addAttribute("response", "Purchase order rejected");
        } else {
            System.out.println("NÜÜD ON PÕRGU LAHTII JOUJOUJOU");
            // some strange error, shouldn't happen
            model.addAttribute("response", "Something went wrong");
        }

        // attach stuff to show in the html
        model.addAttribute("po", newPurchaseOrderDTO);
        model.addAttribute("businessPeriod", bpDTO);

        // imo võiks siia teha if-lause nõnda, et kui on rejected, siis saadetakse algusesse tagasi ja kuvatakse mingi teade
        return "dashboard/catalog/order";
    }

}