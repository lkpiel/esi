package com.example.sales.web.dto;

import com.example.common.application.dto.BusinessPeriodDTO;
import lombok.*;

/**
 * Created by lkpiel on 3/7/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class CatalogQueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}
