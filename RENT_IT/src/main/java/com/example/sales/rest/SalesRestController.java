package com.example.sales.rest;

import com.example.common.application.exceptions.PlantNotAvailableException;
import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.common.application.exceptions.PurchaseOrderNotFoundException;
import com.example.invoicing.application.service.InvoicingService;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.services.SalesService;
import com.example.sales.domain.model.POStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * Created by Philosoraptor on 13/03/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/sales/orders")
public class SalesRestController {

    @Autowired
    SalesService salesService;
    @Autowired
    InvoicingService invoicingService;


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") String id) throws PurchaseOrderNotFoundException {
        PurchaseOrderDTO purchaseOrder = salesService.findPurchaseOrder(id);
        if (purchaseOrder != null) {
            return purchaseOrder;
        } else {
            throw new PurchaseOrderNotFoundException(id);
        }
    }
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> fetchAllPurchaseOrders() throws IOException, MessagingException {
        return salesService.findAllPurchaseOrders();
    }

    //Used for testing only
    /*@GetMapping("/reservations/{id}")
    public PlantReservationDTO getReservation(@PathVariable("id") String id) throws PurchaseOrderNotFoundException {
        PlantReservationDTO plantReservationDTO = plantReservationAssembler.toResource(plantReservationRepository.findOne(id));

        return plantReservationDTO;
    }*/

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody PurchaseOrderDTO partial)
            throws PlantNotFoundException, PlantNotAvailableException, IOException, MessagingException {
        System.out.println("LOOOOOOOG");
        System.out.println("SEE NIMI " + partial.getPlant().getName());
        PurchaseOrderDTO newlyCreatePODTO = salesService.createPurchaseOrder(partial.getPlant(), partial.getRentalPeriod());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((newlyCreatePODTO.getId().getHref())));

        if (newlyCreatePODTO.getStatus() == POStatus.OPEN || newlyCreatePODTO.getStatus() == POStatus.REJECTED) {
            return new ResponseEntity<PurchaseOrderDTO>(newlyCreatePODTO, headers, HttpStatus.CREATED);
        } else {
            System.out.println("SIIA HOOPIS" + newlyCreatePODTO.getStatus());
            throw new PlantNotAvailableException(newlyCreatePODTO.getPlant().get_id());
        }
    }

    @PostMapping("/{id}/accept")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> acceptPurchaseOrder(@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.OPEN);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO closePurchaseOrder(@PathVariable String id) throws Exception {
        PurchaseOrderDTO closedPurchaseOrderDTO =  salesService.closePurchaseOrder(id);
        return closedPurchaseOrderDTO;
    }

    @PostMapping("/{id}/cancel")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> cancelPurchaseOrder(@PathVariable String id) throws Exception {
        System.out.println("SELLE ID-ga üritan canceldadada" + id);
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.CANCELLED);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }
    @PostMapping("/{id}/dispatched")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> markPurchaseOrderDispatched (@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.DISPATCHED);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }
    @PostMapping("/{id}/delivered")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> markPurchaseOrderDelivered(@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.DELIVERED);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }
    @PostMapping("/{id}/rejectedbycustomer")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> markPurchaseOrderRejectedByCustomer (@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.REJECTED_BY_CUSTOMER);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }
    @PostMapping("/{id}/returned")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> markPurchaseOrderReturned (@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.RETURNED);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }
    @PostMapping("/{id}/returnedinstock")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> markPurchaseOrderReturnedInStock (@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.RETURNED_IN_STOCK);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/accept")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> rejectPurchaseOrder(@PathVariable String id) throws Exception {
        PurchaseOrderDTO purchaseOrderDTO = salesService.updatePurchaseOrderStatus(id, POStatus.REJECTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((purchaseOrderDTO.getId().getHref())));

        return new ResponseEntity<PurchaseOrderDTO>(purchaseOrderDTO, headers, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> resubmitPurchaseOrder(@PathVariable String id, @RequestBody PurchaseOrderDTO purchaseOrderDTO) throws Exception {
        System.out.println("Resubmitting with this PurchaseOrder " + purchaseOrderDTO);
        PurchaseOrderDTO newPurchaseOrderDTO = salesService.updatePurchaseOrder(purchaseOrderDTO);
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<PurchaseOrderDTO>(newPurchaseOrderDTO, headers, HttpStatus.OK);
    }





    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }

    @ExceptionHandler(PlantNotAvailableException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public void handPlantNotAvailableException(PlantNotAvailableException ex) {
    }

    @ExceptionHandler(PurchaseOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPurchaseOrderNotFoundException(PurchaseOrderNotFoundException ex) {
    }

}