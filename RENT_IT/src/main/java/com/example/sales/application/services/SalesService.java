package com.example.sales.application.services;

import com.example.common.application.dto.BusinessPeriodAndPlantEntryDTO;
import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.common.application.services.BusinessPeriodAssembler;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.services.InventoryService;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.invoicing.application.service.InvoicingService;
import com.example.invoicing.application.service.RemindingService;
import com.example.reservations.domain.model.PlantReservation;
import com.example.reservations.domain.repository.PlantReservationRepository;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import com.example.sales.infrastructure.SalesIdentifierFactory;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Philosoraptor on 07/03/2017.
 */
@Service
public class SalesService {
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    PurchaseOrderAssembler poAssembler;
    @Autowired
    PlantInventoryEntryRepository plantRepo;
    @Autowired
    SalesIdentifierFactory idFactory;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    InventoryService inventoryService;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    BusinessPeriodAssembler businessPeriodAssembler;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    InvoicingService invoicingService;
    @Autowired
    RemindingService remindingService;
    public PurchaseOrderDTO createPurchaseOrder(PlantInventoryEntryDTO entryDTO, BusinessPeriodDTO periodDTO) throws PlantNotFoundException, IOException, MessagingException {
        // create a new purchase order item from the given DTO object

        ///////// just for testing 22.03.17 to see what is inside this object
        Gson gson = new Gson();
        BusinessPeriodAndPlantEntryDTO businessPeriodAndPlantEntryDTO = new BusinessPeriodAndPlantEntryDTO();
        businessPeriodAndPlantEntryDTO.setBusinessPeriodDTO(periodDTO);
        businessPeriodAndPlantEntryDTO.setPlant(plantInventoryEntryAssembler.toResource(plantRepo.findOne(entryDTO.get_id())));
        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
        poDTO.setRentalPeriod(periodDTO);
        poDTO.setPlant(plantInventoryEntryAssembler.toResource(plantInventoryEntryRepository.findOne(entryDTO.get_id())));


        PurchaseOrder po = PurchaseOrder.of(idFactory.nextPurchaseOrderID(),null,
                plantRepo.findOne(entryDTO.get_id()),
                LocalDate.now(),null, poDTO.calculateTotalCostForBusinessPeriod(poDTO.getPlant().getPrice()), POStatus.PENDING);
        DataBinder poBinder = new DataBinder(po);
        poBinder.addValidators(new PurchaseOrderValidator());
        poBinder.validate();

        if (!poBinder.getBindingResult().hasErrors()) {
            purchaseOrderRepository.save(po);
        } else {
            purchaseOrderRepository.updatePurchaseOrderStatus(po, POStatus.REJECTED);
        }
        // try to make a new reservation. If there are no items connected with this entry then null will be returned
        PlantReservation reservation;
        try {
             reservation = inventoryService.createPlantReservation(entryDTO.get_id(),
                    periodDTO.getStartDate(), periodDTO.getEndDate(), po);
        } catch (PlantNotFoundException e){
             reservation = null;
        }

        if (reservation != null) {
            // update all the additional information we have gotten in the meantime
            System.out.println("SELLINE RESERVATION SAAB KÜLGE PANDUD "  + gson.toJson(reservation));
            purchaseOrderRepository.updatePurchaseOrderReservation(po, reservation);
            System.out.println("JA SELLINE ON KÜLJES RESERVATION SAAB KÜLGE PANDUD "  + po.getReservation());

            purchaseOrderRepository.updatePurchaseOrderCost(po);

        } else {
            // no items were available for this entry
            // so this inserts an object into the template to be rendered and says that it is now rejected and price is zero
            //purchaseOrderRepository.updatePurchaseOrderCost(po);
            purchaseOrderRepository.updatePurchaseOrderStatus(po, POStatus.REJECTED);

        }

        poBinder.validate();
        if (!poBinder.getBindingResult().hasErrors()) {
            System.out.println("NOT HAS ERRORS NOW!");
            purchaseOrderRepository.updatePurchaseOrderStatus(po, POStatus.OPEN);
        } else {
            System.out.println("HAS ERRORS NOW!");
            purchaseOrderRepository.updatePurchaseOrderStatus(po, POStatus.REJECTED);
        }
        purchaseOrderRepository.save(po);

//
//       System.out.println("MA OLEN PEDEQKK22: " + po.getReservation().getPlant().getId());
//      System.out.println("MA OLEN PEDEQKK44: " + po.getReservation().getId());
        // return a DTO object
        PurchaseOrderDTO newPurchaseOrderDTO = poAssembler.toResource(po);
        newPurchaseOrderDTO.setRentalPeriod(periodDTO);
        return newPurchaseOrderDTO;
    }

    public PurchaseOrderDTO findPurchaseOrder(String id) {
        System.out.println("SEELLINNEEE AIIDIII: " + id);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrderRepository.findOne(id));
        if(purchaseOrderDTO.getReservation() != null){
            purchaseOrderDTO.setRentalPeriod(purchaseOrderDTO.getReservation().getSchedule());
        }
        return purchaseOrderDTO;
    }

    public List<PurchaseOrderDTO> findAllPurchaseOrders() {
        List<PurchaseOrderDTO> purchaseOrderDTOS = purchaseOrderAssembler.toResources(purchaseOrderRepository.findAll());
        for(PurchaseOrderDTO purchaseOrderDTO: purchaseOrderDTOS){
            if(purchaseOrderDTO.getReservation() != null) {
                purchaseOrderDTO.setRentalPeriod(purchaseOrderDTO.getReservation().getSchedule());
            }
        }
        return purchaseOrderDTOS;
    }

    public PurchaseOrderDTO closePurchaseOrder(String id) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(id);
        purchaseOrderRepository.updatePurchaseOrderStatus(purchaseOrder, POStatus.CLOSED);
        purchaseOrderRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }

    public PurchaseOrderDTO updatePurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) throws PlantNotFoundException {
        PurchaseOrder po = purchaseOrderRepository.findOne(purchaseOrderDTO.get_id());
        if(po.getStatus().equals(POStatus.REJECTED)){

            PlantReservation reservation = inventoryService.createPlantReservation(purchaseOrderDTO.getPlant().get_id(),
                    purchaseOrderDTO.getRentalPeriod().getStartDate(), purchaseOrderDTO.getRentalPeriod().getEndDate(), po);

            System.out.println("UPDATING PURCHASE ORDER: " + reservation.getPlant().getPlantInfo().getName().toString());
            if (reservation != null) {

                // update all the additional information we have gotten in the meantime
                purchaseOrderRepository.updatePurchaseOrderReservation(po, reservation);
                purchaseOrderRepository.updatePurchaseOrderCost(po);
                purchaseOrderRepository.updatePurchaseOrderStatus(po, POStatus.PENDING);
                purchaseOrderRepository.save(po);
            } else {

                // no items were available for this entry
                // so this inserts an object into the template to be rendered and says that it is now rejected and price is zero
                //purchaseOrderRepository.updatePurchaseOrderCost(po);

            }
        }
        PurchaseOrderDTO returnPurchaseOrderDTO = poAssembler.toResource(po);
        returnPurchaseOrderDTO.setRentalPeriod(purchaseOrderDTO.getRentalPeriod());

        return returnPurchaseOrderDTO;
    }

    public PurchaseOrderDTO updatePurchaseOrderStatus(String id, POStatus status) throws IOException, MessagingException {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(id);
        if(purchaseOrder.getStatus().equals(POStatus.OPEN)){
            if (status.equals(POStatus.DISPATCHED)||status.equals(POStatus.CANCELLED)){
                purchaseOrder.updateStatus(status);
                if(status.equals(POStatus.CANCELLED)){
                    if(purchaseOrder.getReservation().getSchedule().getStartDate().isAfter(LocalDate.now().plusDays(1l))){
                        plantReservationRepository.delete(purchaseOrder.getReservation());
                        purchaseOrder.updateReservation(null);
                    }
                }
            }
        }
        if(purchaseOrder.getStatus().equals(POStatus.DISPATCHED)){
            if(status.equals(POStatus.DELIVERED)||status.equals(POStatus.REJECTED_BY_CUSTOMER)) {
                purchaseOrder.updateStatus(status);
            }
        }
        if(purchaseOrder.getStatus().equals(POStatus.DELIVERED)){
            if (status.equals(POStatus.RETURNED)){
                purchaseOrder.updateStatus(status);
            }
        }
        if (purchaseOrder.getStatus().equals(POStatus.INVOICED)||
                   purchaseOrder.getStatus().equals(POStatus.REJECTED_BY_CUSTOMER)||
                   purchaseOrder.getStatus().equals(POStatus.REJECTED)) {
            if (status.equals(POStatus.CLOSED)){
                purchaseOrder.updateStatus(status);
            }
        }
        if (status.equals(POStatus.RETURNED)){
            invoicingService.sendInvoiceOut(purchaseOrderAssembler.toResource(purchaseOrder));
            purchaseOrder.updateStatus(POStatus.INVOICED);
        }


        purchaseOrderRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }
}
