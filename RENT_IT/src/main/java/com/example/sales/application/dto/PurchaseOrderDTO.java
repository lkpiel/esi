package com.example.sales.application.dto;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.reservations.application.dto.PlantReservationDTO;
import com.example.sales.domain.model.POStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Created by Philosoraptor on 07/03/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class PurchaseOrderDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    PlantInventoryEntryDTO plant;
    PlantReservationDTO reservation;
    BusinessPeriodDTO rentalPeriod;
    LocalDate issueDate;
    LocalDate paymentSchedule;
    BigDecimal total;
    POStatus status;

    public BigDecimal calculateTotalCostForBusinessPeriod(BigDecimal price) {
        int numberOfWorkingDays = 0;
        for (LocalDate date = this.rentalPeriod.getStartDate(); date.isBefore(this.getRentalPeriod().getEndDate().plusDays(1)); date = date.plusDays(1)) {
            DayOfWeek dow = date.getDayOfWeek();
            if (!(dow.equals(DayOfWeek.SATURDAY) || dow.equals(DayOfWeek.SUNDAY))) {
                numberOfWorkingDays += 1;
            }
        }
        return price.multiply(BigDecimal.valueOf(numberOfWorkingDays));
    }

}
