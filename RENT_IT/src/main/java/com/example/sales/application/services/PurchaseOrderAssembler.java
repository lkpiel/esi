package com.example.sales.application.services;

import com.example.common.rest.ExtendedLink;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.reservations.application.dto.PlantReservationDTO;
import com.example.reservations.application.services.PlantReservationAssembler;
import com.example.reservations.domain.model.PlantReservation;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.infrastructure.SalesIdentifierFactory;
import com.example.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;

/**
 * Created by Meri on 08-Mar-17.
 */
@Service
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {
    @Autowired
    SalesIdentifierFactory idFactory;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PlantReservationAssembler plantReservationAssembler;

    public PurchaseOrderAssembler() {
        super(SalesRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toResource(PurchaseOrder po) {
        if(po != null) {
            PurchaseOrderDTO dto = createResourceWithId(po.getId(), po);
            dto.set_id(po.getId());
            if(po.getPlant() != null){
                dto.setPlant(plantInventoryEntryAssembler.toResource(po.getPlant()));
            }else {
                dto.setPlant(null);
            }
            dto.setReservation(plantReservationAssembler.toResource(po.getReservation()));
            dto.setIssueDate(po.getIssueDate());
            dto.setPaymentSchedule(po.getPaymentSchedule());
            dto.setTotal(po.getTotal());
            dto.setStatus(po.getStatus());
            try {
                switch (po.getStatus()) {
                    case PENDING:
                        dto.add(new ExtendedLink(
                                linkTo(methodOn(SalesRestController.class)
                                        .acceptPurchaseOrder(dto.get_id())).toString(),
                                "accept", POST));
                        dto.add(new ExtendedLink(
                                linkTo(methodOn(SalesRestController.class)
                                        .rejectPurchaseOrder(dto.get_id())).toString(),
                                "reject", DELETE));
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {}
            return dto;
        }else{
            return null;
        }
    }

    public List<PurchaseOrderDTO> toResources(List<PurchaseOrder> purchaseOrders) {
        if(purchaseOrders.size() < 1){
            return new ArrayList<PurchaseOrderDTO>();
        }
        return purchaseOrders.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }


    public PurchaseOrder toEntity(PurchaseOrderDTO bpDTO) {

        PlantReservationDTO reservationDTO = bpDTO.getReservation();
        PlantReservation plantReservation = plantReservationAssembler.toEntity(reservationDTO);

        PurchaseOrder purchaseOrder = PurchaseOrder.of(bpDTO.get_id(),plantReservation,
                plantInventoryEntryAssembler.toEntity(bpDTO.getPlant()),
                LocalDate.now(),null, BigDecimal.ZERO, POStatus.PENDING);

        return purchaseOrder;
    }

}
