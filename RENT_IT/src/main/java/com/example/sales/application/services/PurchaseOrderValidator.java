package com.example.sales.application.services;

import com.example.common.application.services.BusinessPeriodValidator;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

/**
 * Created by Meri on 13-Mar-17.
 */
public class PurchaseOrderValidator implements Validator {
    public boolean supports(Class<?> aClass) {
        return PurchaseOrder.class.equals(aClass);
    }

    public void validate(Object object, Errors errors) {
        BusinessPeriodValidator periodValidator = new BusinessPeriodValidator();
        PurchaseOrder po = (PurchaseOrder) object;
        System.out.println("VALIDATING PURCHASE ORDER ");
        if (po.getId() == null){
            System.out.println("PURCHASE ORDER ID CANNOT BE NULL");
            errors.rejectValue("id", "Purchase Order id cannot be null");
        }

        if (po.getPlant() == null){
            System.out.println("NO PLANT SET FOR PURCHASE ORDER");
            errors.rejectValue("plant.id", "Plant id cannot be null");

        }
        if (!po.getStatus().equals(POStatus.PENDING)) {
            System.out.println("TULIN SIIA JA JÄIN LÕKSU");
            if (po.getReservation() == null) {
                errors.rejectValue("reservation", "Purchase order's reservation cannot be null");
            } else if (po.getReservation().getId() == null){
                errors.rejectValue("reservation.id", "Purchase order's reservation id cannot be null");
            } else {
                errors.pushNestedPath("reservation.schedule");
                ValidationUtils.invokeValidator(periodValidator, po.getReservation().getSchedule(), errors);
                errors.popNestedPath();
            }
        }
        if (po.getTotal().compareTo(BigDecimal.ZERO) == -1) {
            System.out.println("PRICE TOO LOW");
            errors.rejectValue("total", "Purchase order total cost cannot be less than 0");
        }

    }
}