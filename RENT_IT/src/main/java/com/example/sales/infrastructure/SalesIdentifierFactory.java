package com.example.sales.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Philosoraptor on 06/03/2017.
 */
@Service
public class SalesIdentifierFactory {
    public String nextPurchaseOrderID() {
        return UUID.randomUUID().toString();
    }
}
