package com.example.sales.domain.model;

/**
 * Created by lkpiel on 2/18/2017.
 */
public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED, DISPATCHED, CANCELLED, REJECTED_BY_CUSTOMER, RETURNED, RETURNED_IN_STOCK, DELIVERED, INVOICED
}
