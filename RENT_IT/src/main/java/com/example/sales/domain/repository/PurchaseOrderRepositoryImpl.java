package com.example.sales.domain.repository;

import com.example.reservations.domain.model.PlantReservation;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Meri on 08-Mar-17.
 */
public class PurchaseOrderRepositoryImpl implements CustomPurchaseOrderMethods {

    @Override
    public void updatePurchaseOrderStatus(PurchaseOrder po, POStatus status) {
        po.updateStatus(status);
    }

    @Override
    public void updatePurchaseOrderCost(PurchaseOrder po) {
        po.updateTotalCost(po.getPlant().getPrice());
    }

    @Override
    public void updatePurchaseOrderReservation(PurchaseOrder po, PlantReservation pr) {
        po.updateReservation(pr);
    }
}
