package com.example.sales.domain.model;

import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.reservations.domain.model.PlantReservation;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class PurchaseOrder {
    @Id
    String id;
    @OneToOne(cascade = CascadeType.ALL)
    PlantReservation reservation;
    @ManyToOne
    PlantInventoryEntry plant;
    LocalDate issueDate;
    LocalDate paymentSchedule;
    @Column(precision = 8, scale = 2)
    BigDecimal total;
    @Enumerated(EnumType.STRING)
    POStatus status;


    public void updateTotalCost(BigDecimal price) {
        int numberOfWorkingDays = 0;
        if (reservation != null) {
            for (LocalDate date = reservation.getSchedule().getStartDate(); date.isBefore(reservation.getSchedule().getEndDate().plusDays(1)); date = date.plusDays(1)) {
                DayOfWeek dow = date.getDayOfWeek();
                if (!(dow.equals(DayOfWeek.SATURDAY) || dow.equals(DayOfWeek.SUNDAY))) {
                    numberOfWorkingDays += 1;
                }
            }

            this.total = price.multiply(BigDecimal.valueOf(numberOfWorkingDays));
        }
    }

    public void updateReservation(PlantReservation pr) {
        this.reservation = pr;
    }

    public void updateStatus(POStatus pos) {
        this.status = pos;
    }
    public void updatePlant(PlantInventoryEntry plantInventoryEntry){this.plant = plantInventoryEntry;}
}