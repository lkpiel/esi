package com.example.sales.domain.repository;

import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lkpiel on 2/18/2017.
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, String>, CustomPurchaseOrderMethods {
}
