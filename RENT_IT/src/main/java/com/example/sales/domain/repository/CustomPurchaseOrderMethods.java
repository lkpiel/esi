package com.example.sales.domain.repository;

import com.example.reservations.domain.model.PlantReservation;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;

/**
 * Created by Meri on 08-Mar-17.
 */
public interface CustomPurchaseOrderMethods {
    void updatePurchaseOrderStatus(PurchaseOrder po, POStatus status);
    void updatePurchaseOrderCost(PurchaseOrder po);
    void updatePurchaseOrderReservation(PurchaseOrder po, PlantReservation pr);
}
