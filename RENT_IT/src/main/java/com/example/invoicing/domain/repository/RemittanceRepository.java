package com.example.invoicing.domain.repository;

import com.example.invoicing.domain.model.Remittance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Repository
public interface RemittanceRepository extends JpaRepository<Remittance, String> {
}
