package com.example.invoicing.domain.model;

import com.example.sales.domain.model.PurchaseOrder;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDate;

/**
 * Created by lkpiel on 5/2/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Invoice {
    @Id
    String id;
    LocalDate dueDate;
    @OneToOne
    PurchaseOrder purchaseOrder;
    InvoiceStatus invoiceStatus;
}
