package com.example.invoicing.domain.model;

/**
 * Created by lkpiel on 5/2/2017.
 */
public enum InvoiceStatus {
    APPROVED, PENDING, PAID, REJECTED, OVERDUE
}
