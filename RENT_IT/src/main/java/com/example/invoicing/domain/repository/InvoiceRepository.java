package com.example.invoicing.domain.repository;

import com.example.invoicing.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 5/2/2017.
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, String> {
}
