package com.example.invoicing.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by lkpiel on 5/2/2017.
 */
@Service
public class InvoiceIdentifierFactory {
    public String nextInvoiceID() {
        return UUID.randomUUID().toString();
    }
}
