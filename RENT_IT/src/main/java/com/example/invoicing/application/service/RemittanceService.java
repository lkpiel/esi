package com.example.invoicing.application.service;

import com.example.invoicing.application.dto.RemittanceDTO;
import com.example.invoicing.domain.model.InvoiceStatus;
import com.example.invoicing.domain.repository.InvoiceRepository;
import com.example.invoicing.domain.repository.RemittanceRepository;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Service
public class RemittanceService {
    @Autowired
    RemittanceRepository remittanceRepository;
    @Autowired
    RemittanceAssembler remittanceAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    InvoiceRepository invoiceRepository;

    public void processRemittance(RemittanceDTO remittanceDTO) {
        if(remittanceDTO != null){
            System.out.println("THIS IS THE RECEIVED REMITTANCE: " + remittanceDTO.toString());
            System.out.println("THIS IS THE CORRESPONDING PO BEFORE REPO " +remittanceDTO.getInvoiceDTO().getPurchaseOrderDTO());

            PurchaseOrder po = purchaseOrderRepository.findOne(remittanceDTO.getInvoiceDTO().getPurchaseOrderDTO().get_id());
            PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(po);
            remittanceDTO.getInvoiceDTO().setPurchaseOrderDTO(purchaseOrderDTO);
            remittanceDTO.getInvoiceDTO().setInvoiceStatus(InvoiceStatus.PAID);
            remittanceRepository.save(remittanceAssembler.toEntity(remittanceDTO));
        }

    }
}
