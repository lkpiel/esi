package com.example.invoicing.application.service;


import com.example.invoicing.application.dto.InvoiceDTO;
import com.example.invoicing.domain.model.Invoice;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.example.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 5/2/2017.
 */
@Service
public class  InvoiceAssembler  extends ResourceAssemblerSupport<Invoice, InvoiceDTO> {
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    public InvoiceAssembler() {
        super(SalesRestController.class, InvoiceDTO.class);
    }

    public InvoiceDTO toResource(Invoice invoice) {
        if (invoice != null) {
            InvoiceDTO dto = createResourceWithId(invoice.getId(), invoice);
            dto.set_id(invoice.getId());
            dto.setDueDate(invoice.getDueDate());
            dto.setInvoiceStatus(invoice.getInvoiceStatus());
            dto.setPurchaseOrderDTO(purchaseOrderAssembler.toResource(invoice.getPurchaseOrder()));
            return dto;
        }
        return null;
    }
    public Invoice toEntity(InvoiceDTO invoiceDTO) {
        if(invoiceDTO != null) {
            Invoice invoice = Invoice.of(invoiceDTO.get_id(),invoiceDTO.getDueDate(),purchaseOrderAssembler.toEntity(invoiceDTO.getPurchaseOrderDTO()),invoiceDTO.getInvoiceStatus());
            return invoice;
        }
        return null;
    }
}
