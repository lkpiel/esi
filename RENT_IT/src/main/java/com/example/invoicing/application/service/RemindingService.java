package com.example.invoicing.application.service;

import com.example.invoicing.application.dto.InvoiceDTO;
import com.example.invoicing.domain.model.Invoice;
import com.example.invoicing.domain.model.InvoiceStatus;
import com.example.invoicing.domain.repository.InvoiceRepository;
import com.example.invoicing.infrastructure.InvoiceIdentifierFactory;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Kaur on 5/24/2017.
 */

@MessagingGateway
interface RemindingGateway {
    @Gateway(requestChannel = "sendReminderChannel")
    public void sendReminder(MimeMessage msg);
}

@Service
public class RemindingService {
    @Autowired
    RemindingGateway remindingGateway;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    //Sending a reminder every Friday at 18:00
    @Scheduled(cron = "0 0 18 * * FRI")
    public void sendReminderOut() throws MessagingException, IOException {

        JavaMailSender mailSender = new JavaMailSenderImpl();
        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(rootMessage, true);
        helper.setFrom("esikuningas@gmail.com"); // Add the actual email addresses
        helper.setTo("esikuningas@gmail.com");   // (also here)
        helper.setSubject("invoice Purchase Order nr "); // Check the spelling the subject
        helper.setText("This is a reminder!");

        //Sending a reminder for each Purchase Order that is not paid or closed
        List<PurchaseOrder> purchaseOrderList =  purchaseOrderRepository.findAll();
        for (PurchaseOrder po : purchaseOrderList){
            if (!po.getStatus().equals(POStatus.CLOSED)&&!po.getStatus().equals(POStatus.INVOICED)){
                remindingGateway.sendReminder(rootMessage);
                System.out.println("Scheduler  is working!");
            }
        }

    }

}
