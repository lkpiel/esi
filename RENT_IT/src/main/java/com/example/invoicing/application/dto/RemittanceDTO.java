package com.example.invoicing.application.dto;

import com.example.common.rest.ResourceSupport;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Data
public class RemittanceDTO extends ResourceSupport {
    String _id;
    String text;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate dateSent;

    InvoiceDTO invoiceDTO;
}
