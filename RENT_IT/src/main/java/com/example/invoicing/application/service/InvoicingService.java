package com.example.invoicing.application.service;

import com.example.invoicing.application.dto.InvoiceDTO;
import com.example.invoicing.domain.model.Invoice;
import com.example.invoicing.domain.model.InvoiceStatus;
import com.example.invoicing.domain.repository.InvoiceRepository;
import com.example.invoicing.infrastructure.InvoiceIdentifierFactory;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by lkpiel on 5/2/2017.
 */
@MessagingGateway
interface InvoicingGateway {
    @Gateway(requestChannel = "sendInvoiceChannel")
    public void sendInvoice(MimeMessage msg);

    @Gateway(requestChannel = "sendNotificationChannel")
    public void sendNotification(MimeMessage msg);

}
@Service
public class InvoicingService {
    @Autowired
    InvoicingGateway invoicingGateway;
    @Autowired
    InvoiceIdentifierFactory identifierFactory;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    InvoiceRepository invoiceRepository;
    ObjectMapper mapper;
    public void processInvoice(InvoiceDTO invoice) {
        System.out.println("Will process invoice: " + invoice);
    }

    public void sendInvoiceOut(PurchaseOrderDTO purchaseOrderDTO) throws MessagingException, IOException {
        Invoice invoice = Invoice.of(identifierFactory.nextInvoiceID(), LocalDate.now().plusWeeks(2l), purchaseOrderAssembler.toEntity(purchaseOrderDTO), InvoiceStatus.PENDING);
        invoiceRepository.save(invoice);
        Gson gson = new Gson();
        String purchaseOrderAsString = gson.toJson(purchaseOrderDTO);

        JavaMailSender mailSender = new JavaMailSenderImpl();
        String invoice1 =
                "{\n" +
                        "  \"_id\":\"" + invoice.getId() + "\",\n" +
                        "  \"amount\":"+purchaseOrderDTO.getTotal()+",\n" +
                        "  \"dueDate\": \"" + invoice.getDueDate()+"\",\n" +
                        "  \"purchaseOrderDTO\":" + purchaseOrderAsString + ",\n" +
                        "  \"invoiceStatus\":\"" + InvoiceStatus.PENDING + "\"\n" +
                        "}\n";

        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(rootMessage, true);
        helper.setFrom("esikuningas@gmail.com"); // Add the actual email addresses
        helper.setTo("esikuningas@gmail.com");   // (also here)
        helper.setSubject("invoice Purchase Order nr " + purchaseOrderDTO.get_id()); // Check the spelling the subject
        helper.setText("Dear customer,\n\nPlease find attached the Invoice corresponding to your Purchase Order 123.\n\nKindly yours,\n\nRentIt Team!");

        helper.addAttachment("invoice-po-123.json", new ByteArrayDataSource(invoice1, "application/json"));

       invoicingGateway.sendInvoice(rootMessage);
    }

    public void sendNotificationOut(PurchaseOrderDTO purchaseOrderDTO, String text) throws MessagingException, IOException {


        JavaMailSender mailSender = new JavaMailSenderImpl();


        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(rootMessage, true);
        helper.setFrom("esikuningas@gmail.com"); // Add the actual email addresses
        helper.setTo("esikuningas@gmail.com");   // (also here)
        helper.setSubject("Notification about Purchase Order nr" + purchaseOrderDTO.get_id()); // Check the spelling the subject
        helper.setText("Dear customer,\n\n "+text +",\n\nRentIt Team!");


        invoicingGateway.sendInvoice(rootMessage);
    }
}
