package com.example.invoicing.application.dto;

import com.example.common.rest.ResourceSupport;
import com.example.invoicing.domain.model.InvoiceStatus;
import com.example.sales.application.dto.PurchaseOrderDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by lkpiel on 5/2/2017.
 */
@Data
public class InvoiceDTO  extends ResourceSupport {
    String _id;
    BigDecimal amount;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate dueDate;
    PurchaseOrderDTO purchaseOrderDTO;
    InvoiceStatus invoiceStatus;
}
