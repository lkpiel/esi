package com.example.invoicing.application.service;

import com.example.invoicing.application.dto.RemittanceDTO;
import com.example.invoicing.domain.model.Remittance;
import com.example.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Service
public class RemittanceAssembler extends ResourceAssemblerSupport<Remittance, RemittanceDTO> {
    @Autowired
    InvoiceAssembler invoiceAssembler;
    public RemittanceAssembler() {
        super(SalesRestController.class, RemittanceDTO.class);
    }

    public RemittanceDTO toResource(Remittance remittance) {
        if (remittance != null) {
            RemittanceDTO dto = createResourceWithId(remittance.getId(), remittance);
            dto.set_id(remittance.getId());
            dto.setDateSent(remittance.getDateSent());
            dto.setInvoiceDTO(invoiceAssembler.toResource(remittance.getInvoice()));
            dto.setText(remittance.getText());
            return dto;
        }
        return null;
    }
    public Remittance toEntity(RemittanceDTO remittanceDTO) {
        if(remittanceDTO != null) {
            Remittance remittance = Remittance.of(remittanceDTO.get_id(),remittanceDTO.getText(),remittanceDTO.getDateSent(),invoiceAssembler.toEntity(remittanceDTO.getInvoiceDTO()));
            return remittance;
        }
        return null;
    }
}


