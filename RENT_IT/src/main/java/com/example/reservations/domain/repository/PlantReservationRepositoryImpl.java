package com.example.reservations.domain.repository;

import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lkpiel on 3/8/2017.
 */
public class PlantReservationRepositoryImpl implements CustomReservationRepository {
    @Autowired
    EntityManager em;

    @Override
    public List<PlantInventoryItem> findAvailableItems(String entryID, LocalDate startDate, LocalDate endDate)  {
        List<Object> items =  em.createQuery("SELECT item FROM PlantInventoryItem item WHERE " +
                "(item.plantInfo.id = ?1 AND (item.equipmentCondition = 'SERVICEABLE') " +
                "OR  (item.equipmentCondition = 'UNSERVICEABLE_REPAIRABLE' AND (?4 > ?5 )))" +
                "AND item NOT IN (SELECT r.plant FROM PlantReservation r WHERE (?2 <= r.schedule.endDate) AND (?3 >= r.schedule.startDate))")
                .setParameter(1, entryID).setParameter(2, startDate).setParameter(3, endDate).setParameter(4, startDate).setParameter(5, LocalDate.now().plusWeeks(3l)).getResultList();
        List<PlantInventoryItem> resultItems = new ArrayList<>();
        for (Object result : items)
            resultItems.add((PlantInventoryItem) result);

        System.out.println("----------------------------------- " + resultItems.size());
        return resultItems;
    }

    @Override
    public PurchaseOrder findOverlapingPO(PlantInventoryItem plant, LocalDate startDate, LocalDate endDate) {
        List<Object>  object = em.createQuery("SELECT po FROM PurchaseOrder po WHERE po.reservation.schedule.startDate <= ?1 AND ?2 <= po.reservation.schedule.endDate AND po.reservation.plant = ?3")
                .setParameter(1,endDate).setParameter(2,startDate).setParameter(3, plant).getResultList();
        if(object.size() > 0 ) {
            PurchaseOrder po = (PurchaseOrder) object.get(0);
            return po;
        } else {
            return null;
        }
    }


}
