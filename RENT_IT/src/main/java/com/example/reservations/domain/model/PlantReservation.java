package com.example.reservations.domain.model;

import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.domain.model.PlantInventoryItem;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

// This class is to know which item is rented out
// Annotate this class with JPA & LOMBOK relevant information

@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(exclude={"purchaseOrder"})
public class PlantReservation {
    // Mark this property as @Id and specify that you want JPA to use an ID generator
    @Id
    String id;
    // Refactor these two properties to BusinessPeriod (Since BusinessPeriod is a value object, you must declare the new property as Embedded)
    @Embedded
    BusinessPeriod schedule;

    // Specify the cardinality and direction of the relationship
    @ManyToOne
    PlantInventoryItem plant;

    String maintenanceHref;

    public void updatePlant(PlantInventoryItem plantInventoryItem){this.plant = plantInventoryItem;}



}
