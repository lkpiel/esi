package com.example.reservations.domain.repository;

import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.reservations.domain.model.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lkpiel on 2/17/2017.
 */
@Repository
public interface PlantReservationRepository extends JpaRepository<PlantReservation, String>, CustomReservationRepository {
}
