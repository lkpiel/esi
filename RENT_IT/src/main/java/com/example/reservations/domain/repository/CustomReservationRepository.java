package com.example.reservations.domain.repository;

import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.sales.domain.model.PurchaseOrder;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 3/8/2017.
 */
public interface CustomReservationRepository {
    List<PlantInventoryItem> findAvailableItems(String entryID, LocalDate startDate, LocalDate endDate);
    PurchaseOrder findOverlapingPO(PlantInventoryItem plant, LocalDate startDate, LocalDate endDate);

}
