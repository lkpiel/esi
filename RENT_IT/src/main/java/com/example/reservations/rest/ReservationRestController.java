package com.example.reservations.rest;

import com.example.common.application.dto.MaintenanceTaskDTO;
import com.example.reservations.application.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created by lkpiel on 5/25/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/reservations")
public class ReservationRestController {
    @Autowired
    ReservationService reservationService;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public String planMaintenance(@RequestBody MaintenanceTaskDTO maintenanceTaskDTO) throws IOException, MessagingException {
        String returnMaintenanceTaskDTO = reservationService.planMaintenance(maintenanceTaskDTO);
        return returnMaintenanceTaskDTO;
    }
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public String getMaintenance(@RequestBody MaintenanceTaskDTO maintenanceTaskDTO) throws IOException, MessagingException {
        String returnMaintenanceTaskDTO = reservationService.planMaintenance(maintenanceTaskDTO);
        return returnMaintenanceTaskDTO;
    }

}
