package com.example.reservations.application.services;

import com.example.common.application.services.BusinessPeriodAssembler;
import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.application.services.PlantInventoryEntryAssembler;
import com.example.inventory.application.services.PlantInventoryItemAssembler;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.reservations.application.dto.PlantReservationDTO;
import com.example.reservations.domain.model.PlantReservation;
import com.example.reservations.infrastructure.ReservationsIdentifierFactory;
import com.example.sales.application.services.PurchaseOrderAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Meri on 08-Mar-17.
 */
@Service
public class PlantReservationAssembler {
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;
    @Autowired
    BusinessPeriodAssembler businessPeriodAssembler;
    @Autowired
    BusinessPeriodAssembler bpAssembler;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    ReservationsIdentifierFactory idFactory;

    public PlantReservationDTO toResource(PlantReservation pr) {
        PlantReservationDTO dto = new PlantReservationDTO();
        if (pr != null) {
            dto.set_id(pr.getId());
            dto.setSchedule(bpAssembler.toResource(pr.getSchedule()));
            dto.setPlant(plantInventoryItemAssembler.toResource(pr.getPlant()));
            dto.setMaintenanceHref(pr.getMaintenanceHref());
        }
        return dto;
    }

    public PlantReservation toEntity(PlantReservationDTO prDTO){
        BusinessPeriod businessPeriod = businessPeriodAssembler.toEntity(prDTO.getSchedule());
        //PlantInventoryEntry plantInventoryEntry = plantInventoryEntryAssembler.toEntity(prDTO.getPlant());

        //List<PlantInventoryItem> plantInventoryItems = plantInventoryItemRepository.findAllByPlantInfo(plantInventoryEntry);
        //PlantInventoryItem plantInventoryItem = plantInventoryItems.get(0);
        PlantInventoryItem plantInventoryItem = plantInventoryItemAssembler.toEntity(prDTO.getPlant());


        PlantReservation plantReservation = PlantReservation.of(idFactory.nextReservationID(),
                businessPeriod, plantInventoryItem, prDTO.getMaintenanceHref());

        return plantReservation;
    }
}
