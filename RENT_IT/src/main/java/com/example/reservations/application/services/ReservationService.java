package com.example.reservations.application.services;

import com.example.common.application.dto.MaintenanceTaskDTO;
import com.example.common.application.services.BusinessPeriodAssembler;
import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.application.services.PlantInventoryItemAssembler;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.inventory.domain.repository.InventoryRepository;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.invoicing.application.service.InvoicingService;
import com.example.reservations.domain.model.PlantReservation;
import com.example.reservations.domain.repository.PlantReservationRepository;
import com.example.reservations.infrastructure.ReservationsIdentifierFactory;
import com.example.sales.application.services.PurchaseOrderAssembler;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

/**
 * Created by lkpiel on 5/25/2017.
 */
@Service
public class ReservationService {

    @Autowired
    BusinessPeriodAssembler businessPeriodAssembler;
    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    InventoryRepository inventoryRepository;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    InvoicingService invoicingService;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantReservationAssembler plantReservationAssembler;
    @Autowired
    ReservationsIdentifierFactory identifierFactory;
    public String planMaintenance(MaintenanceTaskDTO maintenanceTaskDTO) throws IOException, MessagingException {
        BusinessPeriod reservationPeriod = BusinessPeriod.of(maintenanceTaskDTO.getStartDate(), maintenanceTaskDTO.getEndDate());
        PlantInventoryItem reservationPlant = plantInventoryItemRepository.findOne(maintenanceTaskDTO.getItem());
        PlantReservation reservation = PlantReservation.of(identifierFactory.nextReservationID()
                ,reservationPeriod
                ,reservationPlant, maintenanceTaskDTO.getHref());
        PurchaseOrder overlapingPO = plantReservationRepository.findOverlapingPO(reservationPlant, reservationPeriod.getStartDate(), reservationPeriod.getEndDate());
        if (overlapingPO != null){

            //ALSO MUST SEND AN E-MAIL TO CUSTOMER
            System.out.println("FOUND OVERLAPING RESERVATION");
            System.out.println("PLANT BEFORE " + overlapingPO.getPlant().getId() );
            List<PlantInventoryItem> plantInventoryItems = plantReservationRepository.findAvailableItems(overlapingPO.getPlant().getId(), overlapingPO.getReservation().getSchedule().getStartDate(), overlapingPO.getReservation().getSchedule().getEndDate());

            if(plantInventoryItems.size() > 0){
                overlapingPO.getReservation().updatePlant(plantInventoryItems.get(0));
                //SEND NOTIFICATION ABOUT CHANGED PLANT
                System.out.println("CHANGED THE PLANT TO " + plantInventoryItems.get(0).getId());
                invoicingService.sendNotificationOut(purchaseOrderAssembler.toResource(overlapingPO), "We are writing to inform you that the plant of your PO has been changed due to maintenance issues.");

            } else {
                System.out.println("CANCELLED PO BECAUSE THERE IS NO AVAILABLE PLANTS FOR THIS PERIOD");
                overlapingPO.updateStatus(POStatus.CANCELLED);
                //SEND NOTIFICATION THAT THE RESERVATION HAS BEEN CANCELLED BECAUSE THE PLANT WAS NOT FOUND
                invoicingService.sendNotificationOut(purchaseOrderAssembler.toResource(overlapingPO), "We are very sorry to inform you that your Purchase Order has been cancelled due to maintenance issues. If you have paid for your invoice you will be paid back. Please feel free to make a new order.");

            }
            purchaseOrderRepository.save(overlapingPO);
        }

        plantReservationRepository.save(reservation);
        Gson gson = new Gson();
        System.out.println(gson.toJson(reservation));

        return "LÕIN RESERVATIONI";
    }
}
