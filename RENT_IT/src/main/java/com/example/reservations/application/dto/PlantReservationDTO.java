package com.example.reservations.application.dto;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.inventory.application.dto.PlantInventoryItemDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Created by Meri on 08-Mar-17.
 */
@Data
@Getter
@EqualsAndHashCode
public class PlantReservationDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    BusinessPeriodDTO schedule;
    PlantInventoryItemDTO plant;
    String maintenanceHref;

}
