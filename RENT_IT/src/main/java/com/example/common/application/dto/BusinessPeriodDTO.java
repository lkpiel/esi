package com.example.common.application.dto;

import com.example.common.domain.model.BusinessPeriod;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by lkpiel on 3/3/2017.
 */

@Data
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class BusinessPeriodDTO {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;
    public BusinessPeriod asBusinessPeriod() {
        BusinessPeriod bp = BusinessPeriod.of(this.getStartDate(), this.getEndDate());
        return bp;
    }
}