package com.example.common.application.services;

import com.example.common.domain.model.BusinessPeriod;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

/**
 * Created by Meri on 13-Mar-17.
 */
public class BusinessPeriodValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return BusinessPeriod.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BusinessPeriod bp = (BusinessPeriod) o;

        if (bp.getStartDate() == null) {
            errors.rejectValue("startDate", "Business period start date cannot be null");
        }
        if (bp.getEndDate() == null) {
            errors.rejectValue("endDate", "Business period end date cannot be null");
        }
        if (bp.getEndDate().isBefore(bp.getStartDate())) {
            errors.rejectValue("startDate", "Business period start date must be before end date");
            errors.rejectValue("endDate", "Business period start date must be before end date");
        }
        if (bp.getStartDate().isBefore(LocalDate.now())) {
            errors.rejectValue("startDate", "Business period start date must be in  the future");
        }

    }
}
