package com.example.common.application.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class MaintenanceTaskDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    String item;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;
    String href;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;
}
