package com.example.common.application.dto;


import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by Oll on 22/03/2017.
 */

@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode
public class BusinessPeriodAndPlantEntryDTO {
    BusinessPeriodDTO businessPeriodDTO;
    PlantInventoryEntryDTO plant;
}
