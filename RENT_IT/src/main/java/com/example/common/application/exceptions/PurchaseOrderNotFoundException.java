package com.example.common.application.exceptions;

/**
 * Created by Meri on 13-Mar-17.
 */
public class PurchaseOrderNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public PurchaseOrderNotFoundException(String id) {
        super(String.format("Purchase order not found! (Purchase order id: %s)", id));
    }
}
