package com.example.common.application.exceptions;

/**
 * Created by Philosoraptor on 13/03/2017.
 */
public class PlantNotAvailableException extends Exception {

    private static final long serialVersionUID = 1L;

    public PlantNotAvailableException(String id) {
        super(String.format("Plant not available! (Plant id: %s)", id));
    }

}
