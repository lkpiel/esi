package com.example;

import com.example.invoicing.application.dto.RemittanceDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Configuration
public class HTTPRemittanceIntegration {
    @Bean
    IntegrationFlow inboundHttp() {
        return IntegrationFlows.from(
                Http.inboundChannelAdapter("/api/remittances/remittance")
                        .requestPayloadType(RemittanceDTO.class))
                .handle("remittanceService", "processRemittance")
                .get();
    }
}
