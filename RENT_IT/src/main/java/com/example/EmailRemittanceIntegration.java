package com.example;

import com.example.invoicing.application.dto.RemittanceDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.mail.Mail;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

/**
 * Created by lkpiel on 5/3/2017.
 */
@Configuration
public class EmailRemittanceIntegration {
    String gmailUsername = "esikuningas";
    String gmailPassword = "esikuningas123";
    @Autowired
    @Qualifier("objectMapper")
    ObjectMapper mapper;

    @Bean
    IntegrationFlow inboundMail() {
        return IntegrationFlows.from(Mail.imapInboundAdapter(
                String.format("imaps://%s:%s@imap.gmail.com:993/INBOX", gmailUsername, gmailPassword)
                ).selectorExpression("subject matches '.*remittance.*'"),
                e -> e.autoStartup(true)
                        .poller(Pollers.fixedDelay(5000))
        ).transform("@invoiceProcessor.extractInvoice(payload)")
                .transform(Transformers.fromJson(RemittanceDTO.class, new Jackson2JsonObjectMapper(mapper)))
//                .handle(System.out::println)
                .handle("remittanceService", "processRemittance")
                .get();
    }
}
@Service
class InvoiceProcessor {
    public String extractInvoice(MimeMessage msg) throws Exception {
        Multipart multipart = (Multipart) msg.getContent();
        for (int i = 0; i < multipart.getCount(); i++) {
            BodyPart bodyPart = multipart.getBodyPart(i);
            if (bodyPart.getContentType().contains("json") &&
                    bodyPart.getFileName().startsWith("remittance"))
                return IOUtils.toString(bodyPart.getInputStream(), "UTF-8");
        }
        throw new Exception("oops");
    }
}
