FORMAT: 1A
HOST: http://localhost

# ESI14-RentIt
Excerpt of RentIt's API

# Group Purchase Orders
Notes related resources of the **Plants API**

## Plant Catalog [/api/inventory/plants{?name,startDate,endDate}]
### Retrieve Plants [GET]
+ Parameters
    + name (optional,string) ... Name of the plant
    + startDate (optional,date) ... Starting date for rental
    + endDate (optional,date) ... End date for rental

+ Response 200 (application/json)

        [
          {
            "_id": "2",
            "name": "Mini excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/2"
              }
            },
            "_xlinks": []
          },
          {
            "_id": "1",
            "name": "Mini excavatorrrrrrr",
            "description": "1.5 Tonne Mini excavator",
            "price": 150,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/1"
              }
            },
            "_xlinks": []
          }
        ]
        
        
## Plant [/api/inventory/{id}]
### Get Plant by ID [GET]
+ Parameters
    + id - ID of the desired Plant

+ Response 200 (application/json)

        {
          "_id": "2",
          "name": "Mini excavator",
          "description": "3 Tonne Mini excavator",
          "price": 200,
          "_links": {
            "self": {
              "href": "http://localhost:8090/api/inventory/2"
            }
          },
          "_xlinks": []
        }
        
        

## Purchase Order - Container [/api/sales/orders]
### Create Purchase Order [POST]
+ Request (application/json)

        {  
           "plant":{  
              "_id":"2",
              "name":"Mini excavator",
              "href":"http://localhost:8090/api/inventory/2",
              "_xlinks":[  
        
              ],
              "links":[  
                 {  
                    "rel":"self",
                    "href":"http://localhost:8080/api/procurements/2",
                    "template":{  
                       "variables":{  
                          "variables":[  
        
                          ]
                       },
                       "baseUri":"http://localhost:8080/api/procurements/2"
                    }
                 }
              ]
           },
           "rentalPeriod":{  
              "startDate":{  
                 "year":2017,
                 "month":4,
                 "day":21
              },
              "endDate":{  
                 "year":2017,
                 "month":4,
                 "day":23
              },
              "_xlinks":[  
        
              ],
              "links":[  
        
              ]
           },
           "_xlinks":[  
        
           ],
           "links":[  
        
           ]
        }

+ Response 201 (application/json)

    + Headers
    
            Location: http://192.168.99.100:3000/api/sales/orders/100

    + Body

            {  
               "plant":{  
                  "_id":"2",
                  "name":"Mini excavator",
                  "href":"http://localhost:8090/api/inventory/2",
                  "_xlinks":[  
            
                  ],
                  "links":[  
                     {  
                        "rel":"self",
                        "href":"http://localhost:8080/api/procurements/2",
                        "template":{  
                           "variables":{  
                              "variables":[  
            
                              ]
                           },
                           "baseUri":"http://localhost:8080/api/procurements/2"
                        }
                     }
                  ]
               },
               "rentalPeriod":{  
                  "startDate":{  
                     "year":2017,
                     "month":4,
                     "day":21
                  },
                  "endDate":{  
                     "year":2017,
                     "month":4,
                     "day":23
                  },
                  "_xlinks":[  
            
                  ],
                  "links":[  
            
                  ]
               },
               "_xlinks":[  
            
               ],
               "links":[  
            
               ]
            }





### Retrieve All Purchase Order [GET]

+ Response 200 (application/json)

        [
          {
            "_id": "e181e0bb-6868-4c4a-abb2-5eb3903ee82c",
            "plant": {
              "_id": "2",
              "name": "Mini excavator",
              "description": "3 Tonne Mini excavator",
              "price": 200,
              "_links": {
                "self": {
                  "href": "http://localhost:8090/api/inventory/2"
                }
              },
              "_xlinks": []
            },
            "reservation": {
              "schedule": {
                "startDate": "2017-04-21",
                "endDate": "2017-04-23"
              },
              "plant": null,
              "purchaseOrder": null,
              "_xlinks": []
            },
            "rentalPeriod": null,
            "issueDate": "2017-04-20",
            "paymentSchedule": null,
            "total": 200,
            "status": "OPEN",
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/sales/orders/e181e0bb-6868-4c4a-abb2-5eb3903ee82c"
              }
            },
            "_xlinks": []
          },
          {
            "_id": "9c6f99fa-8fd8-4ae8-9f40-904da41e2384",
            "plant": {
              "_id": "2",
              "name": "Mini excavator",
              "description": "3 Tonne Mini excavator",
              "price": 200,
              "_links": {
                "self": {
                  "href": "http://localhost:8090/api/inventory/2"
                }
              },
              "_xlinks": []
            },
            "reservation": {
              "schedule": {
                "startDate": "2017-04-21",
                "endDate": "2017-04-23"
              },
              "plant": null,
              "purchaseOrder": null,
              "_xlinks": []
            },
            "rentalPeriod": null,
            "issueDate": "2017-04-20",
            "paymentSchedule": null,
            "total": 200,
            "status": "OPEN",
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/sales/orders/9c6f99fa-8fd8-4ae8-9f40-904da41e2384"
              }
            },
            "_xlinks": []
          }
        ]

## Purchase Order - Instance [/api/sales/orders/{id}]
### Retrieve Purchase Order [GET]
+ Parameters
    + id: 100 (required, number) - Purchase order ID in form of a long integer

+ Response 200 (application/json)

        {
          "_id": "7daf9e07-01e8-4c77-b524-ff1dfe40dbd9",
          "plant": {
            "_id": "2",
            "name": "Mini excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/2"
              }
            },
            "_xlinks": []
          },
          "reservation": {
            "schedule": {
              "startDate": "2017-04-21",
              "endDate": "2017-04-22"
            },
            "plant": null,
            "purchaseOrder": null,
            "_xlinks": []
          },
          "rentalPeriod": null,
          "issueDate": "2017-04-20",
          "paymentSchedule": null,
          "total": 200,
          "status": "OPEN",
          "_links": {
            "self": {
              "href": "http://localhost:8090/api/sales/orders/7daf9e07-01e8-4c77-b524-ff1dfe40dbd9"
            }
          },
          "_xlinks": []
        }
        
### Close Purchase Order [DELETE]
+ Parameters
    + id: 100 (required, number) - Purchase order ID in form of a long integer

+ Response 200 (application/json)

        {
          "_id": "c4d56513-6df7-4e53-8def-279f2ab89873",
          "plant": {
            "_id": "2",
            "name": "Mini excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/2"
              }
            },
            "_xlinks": []
          },
          "reservation": {
            "schedule": {
              "startDate": "2017-04-21",
              "endDate": "2017-04-23"
            },
            "plant": null,
            "purchaseOrder": null,
            "_xlinks": []
          },
          "rentalPeriod": null,
          "issueDate": "2017-04-20",
          "paymentSchedule": null,
          "total": 200,
          "status": "CLOSED",
          "_links": {
            "self": {
              "href": "http://localhost:8090/api/sales/orders/c4d56513-6df7-4e53-8def-279f2ab89873"
            }
          },
          "_xlinks": []
        }



## Purchase Order - Accept [/api/sales/orders/{id}/accept]
### Accept Purchase Order [POST]
+ Parameters
    + id: 100 (required, number) - Purchase order ID in form of a long integer

+ Response 200 (application/json)

        {
          "_id": "c4d56513-6df7-4e53-8def-279f2ab89873",
          "plant": {
            "_id": "2",
            "name": "Mini excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/2"
              }
            },
            "_xlinks": []
          },
          "reservation": {
            "schedule": {
              "startDate": "2017-04-21",
              "endDate": "2017-04-23"
            },
            "plant": null,
            "purchaseOrder": null,
            "_xlinks": []
          },
          "rentalPeriod": null,
          "issueDate": "2017-04-20",
          "paymentSchedule": null,
          "total": 200,
          "status": "OPEN",
          "_links": {
            "self": {
              "href": "http://localhost:8090/api/sales/orders/c4d56513-6df7-4e53-8def-279f2ab89873"
            }
          },
          "_xlinks": []
        }
        

### Reject Purchase Order [DELETE]
+ Parameters
    + id: 100 (required, number) - Purchase order ID in form of a long integer

+ Response 200 (application/json)

        {
          "_id": "c4d56513-6df7-4e53-8def-279f2ab89873",
          "plant": {
            "_id": "2",
            "name": "Mini excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "_links": {
              "self": {
                "href": "http://localhost:8090/api/inventory/2"
              }
            },
            "_xlinks": []
          },
          "reservation": {
            "schedule": {
              "startDate": "2017-04-21",
              "endDate": "2017-04-23"
            },
            "plant": null,
            "purchaseOrder": null,
            "_xlinks": []
          },
          "rentalPeriod": null,
          "issueDate": "2017-04-20",
          "paymentSchedule": null,
          "total": 200,
          "status": "REJECTED",
          "_links": {
            "self": {
              "href": "http://localhost:8090/api/sales/orders/c4d56513-6df7-4e53-8def-279f2ab89873"
            }
          },
          "_xlinks": []
        }