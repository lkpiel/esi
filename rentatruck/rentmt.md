FORMAT: 1A
HOST: http://localhost:3030

# ESI16-RentMT
Excerpt of Rent My Truck's API

## Plant Catalog [/rest/plants{?plantName,startDate,endDate}]
### Retrieve Plants [GET]
+ Parameters
    + name (optional,string) ... Name of the plant
    + startDate (optional,date) ... Starting date for rental
    + endDate (optional,date) ... End date for rental

+ Response 200 (application/json)

        {
          "class": [
            "siren4J.collectionResource",
            "collection"
          ],
          "properties": {
            "$siren4j.class": "com.google.code.siren4j.resource.CollectionResource",
            "offset": 0,
            "limit": 0,
            "size": 4
          },
          "entities": [
            {
              "class": ["plant"],
              "rel": ["item"],
              "properties": {
                "$siren4j.class": "com.rentmt.dto.PlantDTO",
                "name": "Bobcat E32i Excavator",
                "id": 1,
                "description": "3.2 Tonne Compact excavator",
                "price": 180
              },
              "links": [
                {
                  "rel": ["self"],
                  "href": "http://localhost:3030/rest/plants/1"
                }
              ]
            },
            {
              "class": ["plant"],
              "rel": ["item"],
              "properties": {
                "$siren4j.class": "com.rentmt.dto.PlantDTO",
                "name": "Bobcat E35 Excavator",
                "id": 2,
                "description": "3.4 Tonne Compact excavator",
                "price": 200
              },
              "links": [
                {
                  "rel": ["self"],
                  "href": "http://localhost:3030/rest/plants/2"
                }
              ]
            },
            {
              "class": ["plant"],
              "rel": ["item"],
              "properties": {
                "$siren4j.class": "com.rentmt.dto.PlantDTO",
                "name": "Bobcat E63 Excavator",
                "id": 3,
                "description": "6.3 Tonne Midi excavator",
                "price": 250
              },
              "links": [
                {
                  "rel": ["self"],
                  "href": "http://localhost:3030/rest/plants/3"
                }
              ]
            },
            {
              "class": ["plant"],
              "rel": ["item"],
              "properties": {
                "$siren4j.class": "com.rentmt.dto.PlantDTO",
                "name": "Case CX75C SR Excavator",
                "id": 4,
                "description": "7.4 Tonne Midi excavator",
                "price": 280
              },
              "links": [
                {
                  "rel": ["self"],
                  "href": "http://localhost:3030/rest/plants/4"
                }
              ]
            }
          ]
        }
