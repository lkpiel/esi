
package com.example;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.scripting.Scripts;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.math.BigDecimal;
import java.time.LocalDate;

@MessagingGateway
interface RentalService {
    @Gateway(requestChannel = "requestChannel", replyChannel = "replyChannel")
    Object findPlants(@Payload String name,
                      @Header("startDate") LocalDate startDate,
                      @Header("startDate") LocalDate endDate);
}

//@Service
//class RentalServiceImpl implements RentalService {
//
//    @Override
//    public Object findPlants(String name, LocalDate startDate, LocalDate endDate) {
//        return new RestTemplate().getForObject("http://localhost:3000/api/inventory/plants", String.class);
//    }
//}
@Data
class PlantInventoryEntryDTO{
    String name;
    String description;
    BigDecimal price;
        }


@SpringBootApplication
public class DemoApplication {

    @Bean
    IntegrationFlow outboundHttpGatewayRentit() {
        return IntegrationFlows.from("requestChannel")
                .handle(Http
                        .outboundGateway("http://localhost:3030/rest/plants")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(String.class))
                .handle(System.out::println)
//               .channel("replyChannel")
                .get();
    }

	@Bean
    IntegrationFlow siren2halTranformation(){
        return IntegrationFlows.from("siren2halChannel")
                .handle(Scripts.script(new ByteArrayResource(
                        (" var obj = JSON.parse(payload);" +
                                "var newObj = obj.entities.map(function (e){ return {name : e.properties.name, " +
                                "description: e.properties.description," +
                                "price: e.properties.price," +
                                "return JSON.stringify(newObj);").getBytes()
                ))
                .lang("javascript"))
                .transform(Transformers.fromJson(PlantInventoryEntryDTO[].class))
                .channel("replyChannel")
                .get();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        RentalService rentalService = context.getBean(RentalService.class);
        System.out.println(rentalService.findPlants("exc", LocalDate.now(), LocalDate.now().plusWeeks(1)));
        System.out.println("System.out.println");
    }
}